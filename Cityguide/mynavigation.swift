//
//  mynavigation.swift
//  Cityguide
//
//  Created by DECODER on 09/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class mynavigation: UINavigationController {
  
    override func viewDidLoad() {
        super.viewDidLoad()
       // configNavigationBar()
        let gradient = CAGradientLayer()
        
        let flareRed = UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1)
        let flareOrange = UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1)
        var bounds = navigationBar.bounds
        bounds.size.height += UIApplication.shared.statusBarFrame.size.height
        gradient.frame = bounds
        gradient.colors = [flareRed.cgColor, flareOrange.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 1)

        if let image = getImageFrom(gradientLayer: gradient) {
          //  navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        }
    }

    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
    func configNavigationBar(){
        navigationBar.barStyle = .default
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = false
        navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        
        navigationBar.tintColor = UIColor.white
        navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UINavigationBar
{
 func applyGradient3(colours: [UIColor]) -> Void {
        self.applyGradient3(colours: colours, locations: nil)
    }
    
 func applyGradient3(colours: [UIColor], locations: [NSNumber]?) -> Void {
//        let gradient: CAGradientLayer = CAGradientLayer()
//       gradient.frame = self.bounds
//        gradient.colors = colours.map { $0.cgColor }
//        gradient.locations = locations
//      self.layer.insertSublayer(gradient, at: 0)
    let gradient: CAGradientLayer = CAGradientLayer()
    var frame = bounds
    frame.size.height += UIApplication.shared.statusBarFrame.size.height
    frame.origin.y -= UIApplication.shared.statusBarFrame.size.height
    gradient.frame = frame
    gradient.colors = colours.map { $0.cgColor }
    gradient.locations = locations
    self.layer.insertSublayer(gradient, at: 0)
    
   // gradient.startPoint = CGPoint(x: 0, y: 0)
   // gradient.endPoint = CGPoint(x: 1, y: 1)
    
    }
}

     

//
//  ServiceManager.swift
//  CityGuideApp
//
//  Created by mac on 26/09/19.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation
import Alamofire

func Print(object: Any) {
    Swift.print(object)
}

class ServiceManager<T:Codable> {
    
    typealias completionHandler = (_ Responce:Codable) -> Void
    
    private(set) var Url = "http://work-demo.xyz/city_guide/"
    private(set) var ClientVersion = "service/service_v2/"
    
    var ServiceName = ""
    var Attachments:[Any]?
    var AttachmentParameter:String?
    var ShowLoader = true
    var Parameters:[String:Any]?

    var HandleResponse = true
    
    private var Manager:SessionManager!
    
    let progress = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.white)
    
    var RootViewController:UIViewController? = {
        
        if #available(iOS 13.0, *)
        {
            let Scenes = UIApplication.shared.connectedScenes
            for scene in Scenes
            {
                if scene.activationState == .foregroundActive
                {
                    let vc = ((scene as! UIWindowScene).delegate as! UIWindowSceneDelegate).window!!.rootViewController
                    if let rootVC = vc{
                        return rootVC
                    }
                    else
                    {
                        return nil
                    }
                }
            }
            return nil
        }
        else
        {
            let app = UIApplication.shared.delegate as! AppDelegate
            return app.window?.rootViewController
        }
    }()
    
    init() {
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        config.urlCache = nil
        Manager = Alamofire.SessionManager(configuration: config)
    }
    
    
    private func ShowProgess()
    {
        let ProgessView = UIViewController.init()
        progress.hidesWhenStopped = true
        progress.color = UIColor.white
        progress.layer.cornerRadius = 10
        progress.frame = CGRect.init(x: 0, y: 0, width: 50, height: 50)
        progress.backgroundColor = UIColor.lightGray
        progress.center = ProgessView.view.center
        ProgessView.view.addSubview(self.progress)
        progress.startAnimating()
        ProgessView.modalPresentationStyle = .custom
        let transitinDelegate = RDProgressView.init()
        ProgessView.transitioningDelegate = transitinDelegate
        RootViewController?.present(ProgessView, animated: false, completion: nil)
        
    }
    
    private func DismissProgress()
    {
        progress.stopAnimating()
        RootViewController?.dismiss(animated: false, completion: nil)
    }
    
    
    func  MakeServiceCall(httpMethod:HTTPMethod,CompletionHandler:@escaping completionHandler)
    {
        if let Attachments = Attachments
        {
            if NetworkReachabilityManager()?.isReachable ?? false
            {
                let finalUrl = self.Url + ClientVersion + ServiceName
                guard let url = URL.init(string: finalUrl) else {
                    return
                }
                Print(object: "URL:-\(finalUrl)")
                if ShowLoader
                {
                    self.ShowProgess()
                }
                Manager.upload(multipartFormData: { (MultipartData) in
                    for attachment in Attachments
                    {
                        let randome = arc4random()
                        if attachment is UIImage
                        {
                            let data = (attachment as! UIImage).jpegData(compressionQuality: 0.75)
                            MultipartData.append(data!, withName: self.AttachmentParameter!, fileName: "randome\(randome).jpg", mimeType: "image/jpg")
                        }
                        else
                        {
                            guard let data = attachment as? Data else {return}
                            let ext = data.mimeType.components(separatedBy: "/").last ?? ""
                            MultipartData.append(data, withName: self.AttachmentParameter!, fileName: "randome\(randome).\(ext)", mimeType: data.mimeType)
                        }
                    }
                    if let Paras = self.Parameters
                    {
                        Print(object: "Parameters:-\(Paras)")
                        for (key,value) in Paras
                        {
                            guard let data = "\(value)".data(using: String.Encoding.utf8) else {
                                return
                            }
                            MultipartData.append(data, withName: key)
                        }
                    }
                }, to: url) { (Response) in
                    if self.ShowLoader
                    {
                        self.DismissProgress()
                    }
                    switch Response
                    {
                    case .success(let upload, _, _):
                        upload.responseData { (Response) in
                            switch Response.result
                            {
                            case.success(let data):
                                let _ = self.Manager //try to remove this line
                                guard let Dic = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) else {return}
                                Print(object: "Responce:-\(Dic as? [String:Any] ?? [:])")
                                let ResponseData = (Dic as? [String:Any])?["data"]
                                var ResponseToSend:GenericResponse
                                do
                                {
                                    if ResponseData is [[String:Any]]
                                    {
                                        ResponseToSend = try JSONDecoder().decode(ResponseModel<T>.self, from: data)
                                        Print(object: ResponseToSend)
                                    }
                                    else if ResponseData is [String:Any]
                                    {
                                        ResponseToSend = try JSONDecoder().decode(ResponseModelDic<T>.self, from: data)
                                        Print(object: ResponseToSend)
                                    }
                                    else
                                    {
                                        ResponseToSend = try JSONDecoder().decode(ResponseModelSuccess.self, from: data)
                                        Print(object: ResponseToSend)
                                    }
                                    
                                    if self.HandleResponse
                                    {
                                        if ResponseToSend.status ?? "" == "1"
                                        {
                                            CompletionHandler(ResponseToSend)
                                        }
                                        else
                                        {
                                           CompletionHandler(ResponseToSend)
                                            UIAlertController().Simplealert(withTitle:ResponseToSend.errorMsg ?? "", Message: "", presentOn: self.RootViewController!)
                                        }
                                    }
                                    else
                                    {
                                        CompletionHandler(ResponseToSend)
                                    }
                                }
                                catch let DecodingError.dataCorrupted(context) {
                                    Print(object: context)
                                } catch let DecodingError.keyNotFound(key, context) {
                                    
                                    Print(object: "Key '\(key)' not found:\(context.debugDescription)")
                                    Print(object: "codingPath:\(context.codingPath)")
                                    
                                } catch let DecodingError.valueNotFound(value, context) {
                                    
                                    Print(object: "Value '\(value)' not found:\(context.debugDescription)")
                                    Print(object: "codingPath:\(context.codingPath)")
                                    
                                } catch let DecodingError.typeMismatch(type, context)  {
                                    
                                    Print(object: "Type '\(type)' mismatch:\(context.debugDescription)")
                                    Print(object: "codingPath:\(context.codingPath)")
                                    
                                } catch {
                                    Print(object: "Error:-\(error.localizedDescription)")
                                }
                            case .failure(let error):
                                Print(object: error.localizedDescription)
                            }
                        }
                    case.failure(let error):
                        Print(object: error.localizedDescription)
                        break
                    }
                }
            }
            else
            {
                
                UIAlertController().Simplealert(withTitle:"No Internet", Message: "", presentOn: self.RootViewController!)
                
              
            
            }
        }
        else
        {
            if NetworkReachabilityManager()?.isReachable ?? false
            {
                let finalUrl = self.Url + ClientVersion + ServiceName
                guard let url = URL.init(string: finalUrl) else {
                    return
                }
                Print(object: "URL:-\(finalUrl)")
                Print(object: "Parameters:-\(self.Parameters ?? [:])")
                if ShowLoader
                {
                    self.ShowProgess()
                }
                Manager.request(url, method: httpMethod, parameters: Parameters).responseData { (Response) in
                    if self.ShowLoader
                    {
                        self.DismissProgress()
                    }
                    switch Response.result
                    {
                    case .success(let data):
                        guard let Dic = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) else {return}
                        Print(object: "Responce:-\(Dic as? [String:Any] ?? [:])")
                        let ResponseData = (Dic as? [String:Any])?["data"]
                        var ResponseToSend:GenericResponse
                        do {
                            if ResponseData is [[String:Any]]
                            {
                                ResponseToSend = try JSONDecoder().decode(ResponseModel<T>.self, from: data)
                                Print(object: ResponseToSend)
                            }
                            else if ResponseData is [String:Any]
                            {
                                ResponseToSend = try JSONDecoder().decode(ResponseModelDic<T>.self, from: data)
                                Print(object: ResponseToSend)
                            }
                            else
                            {
                                ResponseToSend = try JSONDecoder().decode(ResponseModelSuccess.self, from: data)
                                Print(object: ResponseToSend)
                            }
                            
                            if self.HandleResponse
                            {
                                if ResponseToSend.status ?? "" == "1"
                                {
                                    CompletionHandler(ResponseToSend)
                                }
                                else
                                {
                                  CompletionHandler(ResponseToSend)
                                    RDalertcontoller().presentAlertWithMessage(Message:ResponseToSend.errorMsg ?? "" , onVc: self.RootViewController)
                                    
                                    self.RootViewController?.view.shake(values: [-12.0, 12.0, -12.0, 12.0, -6.0, 6.0, -3.0, 3.0, 0.0])
                                    //UIAlertController().Simplealert(withTitle:ResponseToSend.errorMsg ?? "", Message: "", presentOn: self.RootViewController ?? UIViewController())
                               
                                    
                                }
                            }
                            else
                            {
                                CompletionHandler(ResponseToSend)
                            }
                            
                        }
                        catch let DecodingError.dataCorrupted(context) {
                            Print(object: context)
                        } catch let DecodingError.keyNotFound(key, context) {
                            
                            Print(object: "Key '\(key)' not found:\(context.debugDescription)")
                            Print(object: "codingPath:\(context.codingPath)")
                            
                        } catch let DecodingError.valueNotFound(value, context) {
                            
                            Print(object: "Value '\(value)' not found:\(context.debugDescription)")
                            Print(object: "codingPath:\(context.codingPath)")
                            
                        } catch let DecodingError.typeMismatch(type, context)  {
                            
                            Print(object: "Type '\(type)' mismatch:\(context.debugDescription)")
                            Print(object: "codingPath:\(context.codingPath)")
                            
                        } catch {
                            Print(object: "Error:-\(error.localizedDescription)")
                        }
                    case .failure(let error):
                        Print(object: "Error:-\(error)")
                        break
                    }
                }
            }
            else
            {
                UIAlertController().Simplealert(withTitle:"No Internet", Message: "", presentOn: self.RootViewController!)
                
            }
        }
    }
}


extension Data {
    private static let mimeTypeSignatures: [UInt8 : String] = [
        0xFF : "image/jpeg",
        0x89 : "image/png",
        0x47 : "image/gif",
        0x49 : "image/tiff",
        0x4D : "image/tiff",
        0x25 : "application/pdf",
        0xD0 : "application/vnd",
        0x46 : "text/plain",
    ]
    
    var mimeType: String {
        var c: UInt8 = 0
        copyBytes(to: &c, count: 1)
        return Data.mimeTypeSignatures[c] ?? "application/octet-stream"
    }
}


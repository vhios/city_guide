//
//  GolbalShado.swift
//  Beauty spa onwer
//
//  Created by Decoder on 12/07/19.
//  Copyright © 2019 Decoder. All rights reserved.
//

import Foundation
import UIKit


extension UIView {
    
    func Bottom_viewShado(color : UIColor ){
        
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 1
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height:3)
        self.layer.shadowPath = UIBezierPath(rect: CGRect(x: 0,
                                                          y: self.bounds.maxY - self.layer.shadowRadius,
                                                          width: self.bounds.width,
                                                          height: self.layer.shadowRadius)).cgPath
        
    }
    
 
    
}
    

    


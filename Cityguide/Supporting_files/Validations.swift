//
//  Validations.swift
//  Cityguide
//
//  Created by Maitree on 28/01/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    func isValid(testStr:String) -> Bool {
        guard testStr.count >= 2, testStr.count < 30 else { return false }
        
        let predicateTest = NSPredicate(format: "SELF MATCHES %@", "^(([^ ]?)(^[A-Za-z].*[A-Za-z]$)([^ ]?))$")
        return predicateTest.evaluate(with: testStr)
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func isValidPassword(_ str: String) -> Bool {
      //  let passRegEx = "ˆ(?=.*[!@#$&*])(?=.*[a-z].*[a-z].*[a-z]).{8}$"
        let passRegEx = "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8}"

        let passPred = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        return passPred.evaluate(with: str)
    }
    
    func isValidInput(Input:String) -> Bool {
        let RegEx = "^(([^ ]?)(^[a-z].*[a-z]$)([^ ]?))$"
    //    let predicateTest = NSPredicate(format: "SELF MATCHES %@", )
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: Input)
    }
    
    
    func addShadowToTextField(color: UIColor = UIColor.gray, cornerRadius: CGFloat) {
        
        self.backgroundColor = UIColor.white
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 1.0
        //self.backgroundColor = .white
        self.layer.cornerRadius = cornerRadius
    
    }
}
extension UIView{
// Using CAMediaTimingFunction
func shake(duration: TimeInterval = 0.5, values: [CGFloat]) {
    let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")

    // Swift 4.2 and above
    animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)

    // Swift 4.1 and below
    animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)

    animation.duration = duration // You can set fix duration
    animation.values = values  // You can set fix values here also
    self.layer.add(animation, forKey: "shake")
}
}


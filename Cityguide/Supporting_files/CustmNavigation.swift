//
//  CustmNavigation.swift
//  Cityguide
//
//  Created by Maitree on 04/02/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import Foundation
import UIKit

let fontRegular = "Montserrat-Regular"

extension mynavigation{
    
   
    
    func setupNavBack(onVC:UIViewController,withTitle:String?,selector:Selector? = nil,right : Bool,profile : Bool? = nil,left : Bool? = nil)
    {
        
       
        var leftItem = UIBarButtonItem()
        //if selector == nil
       // {
             leftItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "left-arrow"), style: .done, target: self, action: #selector(BackSelected(_:)))
        
       // }
        //else
       // {
       //     leftItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "left-arrow"), style: .done, target: onVC, action: selector)
      //  }
        
       // let leftItem = UIBarButtonItem.init(image: #imageLiteral(resourceName: "left-arrow"), style: .done, target: self, action: #selector(BackSelected(_:)))
        
        if left == true{
            onVC.navigationItem.leftBarButtonItem = leftItem
            onVC.navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsets.init(top: 0, left: 10, bottom: 0, right: 0)
        }
        else{
            
        }
       // onVC.navigationController?.navigationBar.barTintColor = .blue
        
        if let _ = withTitle
        {
            onVC.navigationItem.title = withTitle
            onVC.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font : UIFont.init(name: fontRegular, size: 17)!,NSAttributedString.Key.foregroundColor: UIColor.white]
        }
        
 //    onVC.navigationController?.navigationBar.barTintColor = UIColor.blue
        
      //  onVC.navigationController?.navigationBar.applyGradient3(colours: [UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1),UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1)], locations: [0, 0.8, 1])
        
        onVC.navigationController?.navigationBar.isTranslucent = false
        onVC.navigationController?.navigationBar.tintColor = UIColor.white
        
       if let navFrame = onVC.navigationController?.navigationBar.frame {
           // onVC.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
           // onVC.navigationController?.navigationBar.shadowImage = UIImage()
        
            onVC.navigationController?.navigationBar.layoutIfNeeded()
            let newframe = CGRect(origin: .zero, size: CGSize(width: navFrame.width, height: (navFrame.height + UIApplication.shared.statusBarFrame.height) ))
//
      onVC.navigationController?.navigationBar.barTintColor = UIColor.gradientColor(startColor: UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1), endColor: UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1), frame: newframe)
         //  onVC.navigationController?.navigationBar.barTintColor = UIColor.gradientColor(startColor: UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1), endColor: UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1), frame: newframe)
       }
//
        if right == true{
            var right1 = UIBarButtonItem()
            right1 = UIBarButtonItem.init(image: #imageLiteral(resourceName: "notification"), style: .done, target: self, action: #selector(bellSelected(_:)))
            
            var right = UIBarButtonItem()
            right = UIBarButtonItem.init(image: #imageLiteral(resourceName: "settings"), style: .done, target: self, action: #selector(SettingsSelected(_:)))
            
            if profile == true{
                onVC.navigationItem.rightBarButtonItems = [right,right1]
                           onVC.navigationItem.rightBarButtonItem?.imageInsets = UIEdgeInsets.init(top: 0, left: -10, bottom: 0, right: 0)
            }else{
                onVC.navigationItem.rightBarButtonItem = right1
                onVC.navigationItem.rightBarButtonItem?.imageInsets = UIEdgeInsets.init(top: 0, left: -10, bottom: 0, right: 0)
            }
           
            
        }
        
      //  onVC.navigationController?.navigationBar.barTintColor = NAV_BG_COLOR//UIColor.white
        
//        var updatedFrame = onVC.navigationController?.navigationBar.bounds
//        updatedFrame?.size.height += onVC.navigationController?.navigationBar.frame.origin.y ?? 0
//        let gradienLayer = CAGradientLayer.init(frame: updatedFrame ?? CGRect.zero, colors: setGradientBackgroundColor)
//    onVC.navigationController?.navigationBar.setBackgroundImage(gradienLayer.createGradientImage(), for: .default)
        
    }
    
    func SetupNavSidemenu(onVc:UIViewController,WithTitle:String?)
    {

        if let _ = WithTitle
        {
        onVc.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font : UIFont.init(name: fontRegular, size: 17)!,NSAttributedString.Key.foregroundColor: UIColor.white]
            onVc.navigationItem.title = WithTitle
        }
        
        let leftbarbtn = UIBarButtonItem.init(image: #imageLiteral(resourceName: "menu"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(menuSelected(_:)))
            onVc.navigationItem.leftBarButtonItem = leftbarbtn
        
        onVc.navigationController?.navigationBar.tintColor = UIColor.white
      //  onVc.navigationController?.navigationBar.barTintColor = NAV_BG_COLOR//UIColor.white

        
        if let navFrame = self.navigationController?.navigationBar.frame {
            
            let newframe = CGRect(origin: .zero, size: CGSize(width: navFrame.width, height: (navFrame.height + UIApplication.shared.statusBarFrame.height) ))
            
            onVc.navigationController?.navigationBar.barTintColor = UIColor.blue
            
            
       //     onVc.navigationController?.navigationBar.barTintColor = UIColor.gradientColor(startColor: UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1), endColor: UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1), frame: newframe)
            
           // onVc.navigationController?.navigationBar.applyGradient3(colours: [UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1),UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1)], locations: [0,1])
        }
//        var updatedFrame = onVc.navigationController?.navigationBar.bounds
//        updatedFrame?.size.height += onVc.navigationController?.navigationBar.frame.origin.y ?? 0
//        let gradienLayer = CAGradientLayer.init(frame: updatedFrame ?? CGRect.zero, colors: setGradientBackgroundColor)
//        onVc.navigationController?.navigationBar.setBackgroundImage(gradienLayer.createGradientImage(), for: .default)

        
    }
    
    @objc private func menuSelected(_ button:UIBarButtonItem)
    {
        //sideMenuController?.showLeftView(animated: true, completionHandler: nil)

    }
    
    @objc private func BackSelected(_ button:UIButton)
    {
        self.popViewController(animated: true)
    }
    
    @objc private func bellSelected(_ button:UIButton)
    {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "NotificationVC") as? NotificationVC  else { return }
        self.show(vc, sender: nil)
      
    }
    
    @objc private func SettingsSelected(_ button:UIButton)
       {
              let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                guard let vc = storyboard.instantiateViewController(withIdentifier: "SettingpageVC") as? SettingpageVC  else { return }
                self.show(vc, sender: nil)
                //let navi = UINavigationController(rootViewController: vc)
                //self.present(navi, animated: true, completion: nil)
              //  self.navigationController?.present(vc, animated: true, completion: nil)
                 print("You press search")
                
       }
}

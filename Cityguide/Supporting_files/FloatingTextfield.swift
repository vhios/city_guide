//
//  FloatingTextfield.swift
//  Cityguide
//
//  Created by Maitree on 04/02/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import Foundation
import UIKit

enum placeholderDirection: String {
    case placeholderUp = "up"
    case placeholderDown = "down"
    
}
public class IuFloatingTextFiledPlaceHolder: UITextField {
    var enableMaterialPlaceHolder : Bool = true
    var placeholderAttributes = NSDictionary()
    var lblPlaceHolder = UILabel()
    var defaultFont = UIFont()
    var difference: CGFloat = 40.0
    var directionMaterial = placeholderDirection.placeholderUp
    var isUnderLineAvailabe : Bool = true
    override init(frame: CGRect) {
        super.init(frame: frame)
        Initialize ()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        Initialize ()
    }
    func Initialize(){
       // self.clipsToBounds = false
        self.addTarget(self, action: #selector(IuFloatingTextFiledPlaceHolder.textFieldDidChange), for: .editingChanged)
        self.addTarget(self, action: #selector(IuFloatingTextFiledPlaceHolder.textfieldBeginEditing), for: .editingDidBegin)
        self.addTarget(self, action: #selector(IuFloatingTextFiledPlaceHolder.textfieldEndEditing), for: .editingDidEnd)
        self.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        
        self.borderStyle = .none
        self.layer.cornerRadius = self.frame.size.height / 2
        self.clipsToBounds = true
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height:  self.frame.height))
            self.leftView = paddingView
        self.leftViewMode = UITextField.ViewMode.always
        self.EnableMaterialPlaceHolder(enableMaterialPlaceHolder: true)
        if isUnderLineAvailabe {
            let underLine = UIImageView()
            underLine.backgroundColor = UIColor.init(red: 197/255.0, green: 197/255.0, blue: 197/255.0, alpha: 0.8)
            //            underLine.frame = CGRectMake(0, self.frame.size.height-1, self.frame.size.width, 1)
            underLine.frame = CGRect(x: 0, y: self.frame.size.height-1, width : self.frame.size.width, height : 1)
            
            underLine.clipsToBounds = true
            //self.addSubview(underLine)
        }
        defaultFont = self.font!
        
    }
    @IBInspectable var placeHolderColor: UIColor? = UIColor.lightGray {
        didSet {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder! as String ,
                                                            attributes:[NSAttributedString.Key.foregroundColor: placeHolderColor!])
        }
    }
    
    override public var placeholder:String?  {
        didSet {
            //  NSLog("placeholder = \(placeholder)")
        }
        willSet {
            let atts  = [NSAttributedString.Key.foregroundColor.rawValue: UIColor.lightGray, NSAttributedString.Key.font: UIFont.labelFontSize] as! [NSAttributedString.Key : Any]
            self.attributedPlaceholder = NSAttributedString(string: newValue!, attributes:atts)
            self.EnableMaterialPlaceHolder(enableMaterialPlaceHolder: self.enableMaterialPlaceHolder)
        }
        
    }
    override public var attributedText:NSAttributedString?  {
        didSet {
            //  NSLog("text = \(text)")
        }
        willSet {
            if (self.placeholder != nil) && (self.text != "")
            {
                let string = NSString(string : self.placeholder!)
                self.placeholderText(string)
            }
            
        }
    }
    
    @objc func textfieldBeginEditing(){
        print("email")
        self.layer.borderColor = (UIColor.black.cgColor)
        self.layer.borderWidth = 1
        self.backgroundColor = UIColor.white
        
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 3.0
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        //self.layer.shadowOffset = CGSizeMake(1.0, 1.0)
        self.layer.shadowOpacity = 1.0
    }
    
    @objc func textfieldEndEditing(){
          print("password")
        self.layer.borderColor = (UIColor.white.cgColor)
        self.layer.borderWidth = 0
        self.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        
       self.layer.masksToBounds = false
        self.layer.shadowRadius = 0
        self.layer.shadowColor = UIColor.white.cgColor
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.shadowOpacity = 0
    }
    
    @objc func textFieldDidChange(){
        if self.enableMaterialPlaceHolder {
            if (self.text == nil) || (self.text?.count)! > 0 {
                self.lblPlaceHolder.alpha = 1
                self.attributedPlaceholder = nil
                self.lblPlaceHolder.textColor = self.placeHolderColor
                self.lblPlaceHolder.frame.origin.x = 15 ////\\
                let fontSize = self.font!.pointSize;
                self.lblPlaceHolder.font = UIFont.init(name: (self.font?.fontName)!, size: fontSize-3)
            }
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {() -> Void in
                if (self.text == nil) || (self.text?.count)! <= 0 {
                    self.lblPlaceHolder.font = self.defaultFont
                    self.lblPlaceHolder.frame = CGRect(x: self.lblPlaceHolder.frame.origin.x+10, y : 0, width :self.frame.size.width, height : self.frame.size.height)
                }
                else {
                  
                    if self.directionMaterial == placeholderDirection.placeholderUp {
                        self.lblPlaceHolder.frame = CGRect(x : self.lblPlaceHolder.frame.origin.x, y : -self.difference, width : self.frame.size.width, height : self.frame.size.height)
                    }else{
                        self.lblPlaceHolder.frame = CGRect(x : self.lblPlaceHolder.frame.origin.x , y : self.difference, width : self.frame.size.width, height : self.frame.size.height)
                    }
                    
                }
            }, completion: {(finished: Bool) -> Void in
            })
        }
    }
    
    func EnableMaterialPlaceHolder(enableMaterialPlaceHolder: Bool){
        self.enableMaterialPlaceHolder = enableMaterialPlaceHolder
        self.lblPlaceHolder = UILabel()
        self.lblPlaceHolder.frame = CGRect(x: 10, y : 0, width : 0, height :self.frame.size.height)
        self.lblPlaceHolder.font = UIFont.systemFont(ofSize: 10)
        self.lblPlaceHolder.alpha = 0
        self.lblPlaceHolder.clipsToBounds = true
        self.addSubview(self.lblPlaceHolder)
        self.lblPlaceHolder.attributedText = self.attributedPlaceholder
        
    
        //self.lblPlaceHolder.sizeToFit()
    }
    func placeholderText(_ placeholder: NSString){
        let atts  = [NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: UIFont.labelFontSize] as [NSAttributedString.Key : Any]
        self.attributedPlaceholder = NSAttributedString(string: placeholder as String , attributes:atts)
        self.EnableMaterialPlaceHolder(enableMaterialPlaceHolder: self.enableMaterialPlaceHolder)
    }
    
    override public func becomeFirstResponder()->(Bool){
        let returnValue = super.becomeFirstResponder()
        return returnValue
    }
    override public func resignFirstResponder()->(Bool){
        let returnValue = super.resignFirstResponder()
        return returnValue
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
    }
        @IBInspectable var leftImage:UIImage?{
            didSet{
                if let image = leftImage{
                    self.leftViewMode = .always
                    let image = UIImageView.init(image: image)
                    image.contentMode = .scaleAspectFit
                    image.frame = CGRect(x: 15, y: 15, width: 13, height: 13)
                    self.leftView = image
                    self.layoutIfNeeded()
                }
            }
        }

        @IBInspectable var RightImage:UIImage?{
            didSet{
                if let image = RightImage{
                    self.rightViewMode = .always
                    let image = UIImageView.init(image: image)
                    image.contentMode = .scaleAspectFit
                    image.frame = CGRect(x: 15, y: 15, width: 13, height: 13)
                    self.rightView = image
                    self.layoutIfNeeded()
                }
            }
        }

        @IBInspectable var padding = 20

        @IBInspectable var txtpadding:CGFloat = 40

        var CornerRadias = 8

    override public func leftViewRect(forBounds bounds: CGRect) -> CGRect {
            var rect = super.leftViewRect(forBounds: bounds)
            rect.origin.x = rect.origin.x + CGFloat(padding) - 5
            return rect
        }

    override public func rightViewRect(forBounds bounds: CGRect) -> CGRect {
            var rect = super.rightViewRect(forBounds: bounds)
            rect.origin.x = rect.origin.x - CGFloat(padding) - 12
            return rect
        }
        
        let pading = UIEdgeInsets(top: 0, left: 20 , bottom: 0, right: 35)
    override public func placeholderRect(forBounds bounds: CGRect) -> CGRect {
    //        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsets.init(top: 0, left: txtpadding, bottom: 0, right: 0))
              return bounds.inset(by: pading)
        }
        
        
        
    override public func textRect(forBounds bounds: CGRect) -> CGRect {
    //        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsets.init(top: 0, left: txtpadding, bottom: 0, right: 0))
              return bounds.inset(by: pading)
        }
        
    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
    //        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsets.init(top: 0, left: txtpadding, bottom: 0, right: 0))
              return bounds.inset(by: pading)
        }
        
    override public func awakeFromNib() {
            super.awakeFromNib()
//            self.layer.borderWidth = 0.8
//            self.layer.borderColor = UIColor.lightGray.cgColor
        }
}

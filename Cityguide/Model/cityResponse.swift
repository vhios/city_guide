//
//  cityResponse.swift
//  Cityguide
//
//  Created by Maitree on 04/02/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import Foundation

struct cityData: Codable {
    let id, name, stateID, countryID: String
    let status, allName: String

    enum CodingKeys: String, CodingKey {
        case id, name
        case stateID = "state_id"
        case countryID = "country_id"
        case status
        case allName = "allname"
    }
}

struct addCity: Codable {
    let countryID, stateID, cityID, userID: String
    let cityName, createdDate: String

    enum CodingKeys: String, CodingKey {
        case countryID = "country_id"
        case stateID = "state_id"
        case cityID = "city_id"
        case userID = "user_id"
        case cityName = "city_name"
        case createdDate = "created_date"
    }
}

  
 

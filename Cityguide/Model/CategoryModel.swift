//
//  CategoryModel.swift
//  Cityguide
//
//  Created by Maitree on 06/02/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import Foundation

// MARK: - CategoryDatum
struct CategoryDataResponce: Codable {
    let id, name: String
    let image: String?
    let subCat: [CategoryDataResponce]?
    let userSelected: Int?

    enum CodingKeys: String, CodingKey {
        case id, name, image
        case subCat = "sub_cat"
        case userSelected = "user_selected"
    }
}

typealias CategoryData = [CategoryDataResponce]

//
//  cityModel.swift
//  CityGuideApp
//
//  Created by mac on 01/10/19.
//  Copyright © 2019 mac. All rights reserved.
//

import Foundation

struct city {
    
    let city:String?
    let country:String
    let state:String?
    let pincode:String?
    
}

struct User:Codable {
    
    static var shared:User?
    
    let id: Int
    let name, email, apiToken, homePhone: String
    let cellPhone: String
    let roleid: Int
    let enrollmentCode: String
    
    
    enum CodingKeys: String, CodingKey {
        case id, name, email
        case apiToken = "api_token"
        case homePhone = "home_phone"
        case cellPhone = "cell_phone"
        case roleid
        case enrollmentCode = "enrollment_code"
    }
    
    enum userKeys:String {
        case id = "UserID"
        case Token = "Token"
        case EnrollmentId = "EnrollmentID"
    }
    
    func loginUser()
    {
        UserDefaults.standard.set(self.id, forKey: userKeys.id.rawValue)
        UserDefaults.standard.set(self.apiToken, forKey: userKeys.Token.rawValue)
        UserDefaults.standard.set(self.enrollmentCode, forKey: userKeys.EnrollmentId.rawValue)
    }
    
    func logoutUser()
    {
        UserDefaults.standard.removeObject(forKey: userKeys.id.rawValue)
        UserDefaults.standard.removeObject(forKey: userKeys.Token.rawValue)
        UserDefaults.standard.removeObject(forKey: userKeys.EnrollmentId.rawValue)
    }
    
}

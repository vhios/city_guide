//
//  socialLoginDataResponse.swift
//  Cityguide
//
//  Created by Maitree on 28/01/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import Foundation
struct socialLoginResponse: Codable {
    let id, name, firstName, lastName: String
    let username, email: String
    let profilePic: String
    let loginToken: String

    enum CodingKeys: String, CodingKey {
        case id
        case name = "NAME"
        case firstName = "first_name"
        case lastName = "last_name"
        case username, email
        case profilePic = "profile_pic"
        case loginToken = "login_token"
    }
}


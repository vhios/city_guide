//
//  addRegisterData.swift
//  CityGuideApp
//
//  Created by Maitree on 24/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import Foundation

class RegisterUserData : NSObject
{
    var lastName: String?
    var email: String?
    var firstName: String?
    var password: String?
    var userName: String?
    var Profilepic: String?
    var type : String?
    var deviceid : String?
    var facebookId : String?
    var gogleId : String?
    var appleId : String?
}



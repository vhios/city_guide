//
//  ProfileModel.swift
//  Cityguide
//
//  Created by Maitree on 10/02/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

//
//   let profile = try? newJSONDecoder().decode(Profile.self, from: jsonData)

import Foundation

// MARK: - Profile
struct ProfileData: Codable {
    let id, firstName, lastName, name: String
    let email, password, username, profilePic: String
    let gender, aboutMe, dob: String
    let link: JSONNull?
    let placeForLive: String
    let country, state, city: JSONNull?
    let deviceType, deviceID, regType, facebookID: String
    let gmailID, status, createdDate: String
    let rs: JSONNull?
    let notification: String
    let facebookuser, instagramuser, googleuser, twitteruser: JSONNull?
    let pinterestuser, googleplusUser, userLocation, unread: JSONNull?
    let totalNotification, isAdmin: JSONNull?
    let appleID, loginToken, updatedDate: String

    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case name, email, password, username
        case profilePic = "profile_pic"
        case gender
        case aboutMe = "about_me"
        case dob, link
        case placeForLive = "place_for_live"
        case country, state, city
        case deviceType = "device_type"
        case deviceID = "device_id"
        case regType = "reg_type"
        case facebookID = "facebook_id"
        case gmailID = "gmail_id"
        case status
        case createdDate = "created_date"
        case rs, notification, facebookuser, instagramuser, googleuser, twitteruser, pinterestuser, googleplusUser
        case userLocation = "user_location"
        case unread
        case totalNotification = "total_notification"
        case isAdmin = "is_admin"
        case appleID = "apple_id"
        case loginToken = "login_token"
        case updatedDate = "updated_date"
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

//
//  NotificationVC.swift
//  Cityguide
//
//  Created by DECODER on 07/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var notificationtv: UITableView!
    @IBOutlet weak var Markasbtn: UIButton!
    @IBOutlet weak var showallbtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let button = UIButton(type: UIButton.ButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "left-arrow"), for: .normal)
        button.addTarget(self, action:#selector(callMethod), for: .touchDragInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem(customView: button)
       // self.navigationItem.leftBarButtonItems = [barButton]
        
        self.navigationItem.hidesBackButton = true
     //   self.title = "Notifications"
//        self.navigationItem.leftBarButtonItem = UIImage(named: "left-arrow")
        
        let containerView:UIView = UIView(frame:self.notificationtv.frame)
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.backgroundColor = UIColor.clear
        containerView.layer.shadowOffset = CGSize(width: 1.0, height: 3.0)
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowRadius = 2
        // This is for rounded corners
        containerView.layer.cornerRadius = 10
        
        self.notificationtv.layer.cornerRadius = 10
        self.notificationtv.layer.masksToBounds = true
        self.view.addSubview(containerView)
        containerView.addSubview(self.notificationtv)
        
      // mynavigation().setupNavBack(onVC: self, withTitle: "Terms & Conditions", right: false)

//
//        notificationtv.layer.cornerRadius = 20
//        notificationtv.clipsToBounds = true
       notificationtv.register(UINib(nibName: "Notificationcell", bundle: nil), forCellReuseIdentifier: "Notificationcell")
        notificationtv.delegate = self
        notificationtv.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    
    @objc func callMethod()
    {
        self.navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return 20
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = notificationtv.dequeueReusableCell(withIdentifier: "Notificationcell") as! Notificationcell
        return cell
      }
      
//
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        <#code#>
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

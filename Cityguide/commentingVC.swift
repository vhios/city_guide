//
//  commentingVC.swift
//  Cityguide
//
//  Created by Decoder on 16/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class commentingVC: UIViewController, UITableViewDelegate,
UITableViewDataSource{
   

    @IBOutlet weak var commentview: UIView!
    @IBOutlet weak var commenttxt: RdtextField!
    @IBOutlet weak var profilingimg: UIImageView!
    @IBOutlet weak var profileimg: UIImageView!
    @IBOutlet weak var CommentVC: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileimg.layer.cornerRadius = profileimg.frame.height / 2
        profileimg.clipsToBounds = true
        
        profilingimg.layer.cornerRadius = profilingimg.frame.height / 2
        profilingimg.clipsToBounds = true
        
        
        CommentVC.delegate = self
        CommentVC.dataSource = self
    
        
        commenttxt.layer.borderWidth = 0
        commenttxt.clipsToBounds = true
        
        
        CommentVC.layer.cornerRadius = 15
        CommentVC.clipsToBounds = true
        
        
        CommentVC.register(UINib(nibName: "Commentingcell", bundle: nil), forCellReuseIdentifier: "Commentingcell")
        
        
        self.hidesBottomBarWhenPushed = true
        
        
        commentview.layer.cornerRadius = commentview.frame.height
        / 2

        commentview.clipsToBounds = true
        
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 2
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = CommentVC.dequeueReusableCell(withIdentifier: "Commentingcell") as! Commentingcell
//        cell.inok = "hello"
           return cell
       }
       

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

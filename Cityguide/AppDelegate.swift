//
//  AppDelegate.swift
//  Cityguide
//
//  Created by DECODER on 06/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import GoogleMaps
import GooglePlaces

var LoginFrom = ""
var PostID = ""
var objModelddata : RegisterUserData = RegisterUserData()
var Login:[LoginData]?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let navigationBar = UINavigationBar.appearance()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        GIDSignIn.sharedInstance().clientID = "438195305273-ko5lubduv12n8mliuqpm147n4cslmmid.apps.googleusercontent.com"
        
        GMSServices.provideAPIKey("AIzaSyAY1EVB4-1-6tb-OXGaWbsZl4x10YhZ_cY")
        GMSPlacesClient.provideAPIKey("AIzaSyAY1EVB4-1-6tb-OXGaWbsZl4x10YhZ_cY")
        
        UIFont.familyNames.forEach({ familyName in
            let fontNames = UIFont.fontNames(forFamilyName: familyName)
            print(familyName, fontNames)
        })
        if UserDefaults.standard.value(forKey: "token") as? String ?? "" == ""{
        if UserDefaults.standard.value(forKey: "login") as? Bool ?? false  == true{
            if UserDefaults.standard.value(forKey: "city") as? String ?? ""  != ""{
                if UserDefaults.standard.value(forKey: "interest") as? String ?? ""  != ""{
                    let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mytabbar") as! mytabbar
                    let nav = UINavigationController.init(rootViewController: vc)
                    nav.navigationBar.tintColor = UIColor.white
                    window?.rootViewController = nav
                    window?.makeKeyAndVisible()
                    
                }else{
                    let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "intresetedVC") as! intresetedVC
                    let nav = UINavigationController.init(rootViewController: vc)
                    nav.navigationBar.tintColor = UIColor.white
                    window?.rootViewController = nav
                    window?.makeKeyAndVisible()
                }
            }
            else{
                let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChoosecityVC") as! ChoosecityVC
                
                //  vc.my_profile = "true"
                let nav = UINavigationController.init(rootViewController: vc)
                //nav.navigationBar.barTintColor = NAV_BG_COLOR
                
                //   nav.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
                //  nav.setViewControllers([vc], animated: true)
                window?.rootViewController = nav
                window?.makeKeyAndVisible()
            }
        }
        }
        else{
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            
            //  vc.my_profile = "true"
            let nav = UINavigationController.init(rootViewController: vc)
            //   nav.navigationBar.barTintColor = NAV_BG_COLOR
            nav.navigationBar.tintColor = UIColor.white
            //   nav.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
            //  nav.setViewControllers([vc], animated: true)
            window?.rootViewController = nav
            window?.makeKeyAndVisible()
            
        }
        
        return true
        
        // Override point for customization after application launch.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let DeviceToken = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print(DeviceToken)
        objModelddata.deviceid = DeviceToken
        UserDefaults.standard.set(DeviceToken, forKey: "token")
    }
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let handled = ApplicationDelegate.shared.application(application, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        
        if LoginFrom == "Gmail"
        {
            
            return GIDSignIn.sharedInstance().handle(url)
            
            // return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        }
        else if LoginFrom == "Facebook"
            
        {
            return ApplicationDelegate.shared.application(application, open: url, options: options)
            
            // return ApplicationDelegate.shared.application(application, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        }
        
        return true
        
        // Add any custom logic here.
        // return handled
    }
    
    // MARK: UISceneSession Lifecycle
    
    //    @available(iOS 13.0, *)
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    //    @available(iOS 13.0, *)
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
}


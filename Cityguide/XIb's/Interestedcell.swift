//
//  Interestedcell.swift
//  Cityguide
//
//  Created by DECODER on 06/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class Interestedcell: UITableViewCell, UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
  
    var searchresulting : Bool = false
    
    var array:[String] = ["jatin1346","decoder12346789","cleeee123456","decoder1234689,jatin1346","decoder12346789","cleeee123456","decoder1234689","jatin1346","decoder12346789","cleeee123456","decoder1234689"]
    
    var collectinginfor = [String]()
          
    var addbyyout = String()
    var row = String()

    var searchresult:[String] = []
    var collectionviewlist = [String]()
    //
    @IBOutlet weak var heightofcollectionview: NSLayoutConstraint!
    
    
    @IBOutlet weak var collectionhedinglbl: UILabel!
    @IBOutlet weak var Interestcollection: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        Interestcollection.delegate = self
        Interestcollection.dataSource = self
        self.textLabel?.text = row
    

        Interestcollection.register(UINib(nibName: "Interestedcvc", bundle: nil), forCellWithReuseIdentifier: "Interestedcvc")
 
//        heightofcollectionview.constant =
        // Initialization coded
    }
   
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
        
      }
  
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = Interestcollection.dequeueReusableCell(withReuseIdentifier: "Interestedcvc", for: indexPath) as! Interestedcvc
        
        cell.downloadlbl.text = array[indexPath.row]
        
        
        if #available(iOS 13.0, *) {
            
            cell.contentView.backgroundColor = .placeholderText
        } else {
            // Fallback on earlier versions
        }
        return cell
        
      }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        heightofcollectionview.constant = collectionView.contentSize.height
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       let string = array[indexPath.row]
            
            // your label font.
            let font = UIFont.systemFont(ofSize: 12)
            let fontAttribute = [NSAttributedString.Key.font: font]
            
            // to get the exact width for label according to ur label font and Text.
            let size = string.size(withAttributes: fontAttribute)
              // some extraSpace give if like so.
            let extraSpace : CGFloat =  10.0
            let width = size.width + extraSpace
            return CGSize(width:width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = Interestcollection.cellForItem(at: indexPath) as? Interestedcvc
        
        cell?.downloadlbl.textColor = .white
        cell?.contentView.applyGradient(colours: [UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1),UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1)],locations: [0.0,1.0])
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = Interestcollection.cellForItem(at: indexPath) as? Interestedcvc
        cell?.downloadlbl.textColor = .black
        cell?.contentView.backgroundColor = .lightGray
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  Interestprofilecell.swift
//  Cityguide
//
//  Created by DECODER on 11/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class Interestprofilecell: UITableViewCell {

    @IBOutlet weak var profileimg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        profileimg.layer.cornerRadius = profileimg.frame.height / 2
        profileimg.clipsToBounds = true
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

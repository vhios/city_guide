//
//  email&notificationCell.swift
//  Cityguide
//
//  Created by DECODER on 07/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class emailnotificationCell: UITableViewCell {

    @IBOutlet weak var notificationlbl: UILabel!
    @IBOutlet weak var btnonoff: UIButton!
    @IBOutlet weak var offnotificationimg: UIImageView!
    @IBOutlet weak var onnotificationimg: UIImageView!
    @IBOutlet weak var onOffSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        offnotificationimg.isHidden = true
        onnotificationimg.isHidden = false
        
        // Initialization code
    }

    @IBAction func onoffbtn(_ sender: UIButton) {
         if offnotificationimg.isHidden == true
         {
            offnotificationimg.isHidden = false
            onnotificationimg.isHidden = true
        }
        else
         {
            offnotificationimg.isHidden = true
            onnotificationimg.isHidden = false
            
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

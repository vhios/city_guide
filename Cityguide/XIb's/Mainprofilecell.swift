//
//  Mainprofilecell.swift
//  Cityguide
//
//  Created by DECODER on 11/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class Mainprofilecell: UITableViewCell {

    @IBOutlet weak var editingimg: UIImageView!
    @IBOutlet weak var profileimgs: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profileimgs.layer.cornerRadius = profileimgs.frame.height / 2
        profileimgs.clipsToBounds = true
        
        profileimgs.layer.borderWidth = 2
        profileimgs.layer.borderColor = UIColor.red.cgColor
        editingimg.layer.cornerRadius = editingimg.frame.height / 2
        editingimg.clipsToBounds = true
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}

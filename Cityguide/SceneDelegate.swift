//
//  SceneDelegate.swift
//  Cityguide
//
//  Created by DECODER on 06/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import GoogleSignIn

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    private(set) var Navigation:mynavigation?
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        let navigation = (((UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.window?.rootViewController) as? mytabbar)?.viewControllers?.first as? mynavigation
        self.Navigation = navigation
        
        
        guard let _ = (scene as? UIWindowScene) else { return }
        //
         
        if UserDefaults.standard.value(forKey: "login") as? Bool ?? false  == true{
            
            if UserDefaults.standard.value(forKey: "city") as? String ?? ""  != ""{
                if UserDefaults.standard.value(forKey: "interest") as? String ?? ""  != ""{
                   let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mytabbar") as! mytabbar
                    
                 //   vc.navigationController?.navigationBar.barTintColor = UIColor.red
                  //  let navController = UINavigationController(rootViewController: vc)
                  //  navController.navigationBar.barTintColor = UIColor.red
                    //let forumvc = (vc.viewControllers?.first as? UINavigationController)
                   // vc.selectedIndex = 0
//                    let dashboard =
//                    forumvc?.show(, sender: <#T##Any?#>)

                   window?.rootViewController = vc
                   window?.makeKeyAndVisible()
 
//                     let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
//              //     let journalVC = DashboardVC()
//                     let navController = UINavigationController(rootViewController: vc)
//
//                     let tabBarController = UITabBarController()
//
//                     tabBarController.viewControllers = [navController]
//
//                     window?.rootViewController = tabBarController
//                     window?.makeKeyAndVisible()
//
                    
                }
                else
                {
                    let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "intresetedVC") as! intresetedVC
                    vc.from = "rootview"
                    let nav = UINavigationController.init(rootViewController: vc)
                    nav.navigationBar.tintColor = UIColor.white
                    window?.rootViewController = nav
                    window?.makeKeyAndVisible()
                }
            }
            else
            {
                let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChoosecityVC") as! ChoosecityVC
                
                //  vc.my_profile = "true"
                let nav = UINavigationController.init(rootViewController: vc)
                //nav.navigationBar.barTintColor = NAV_BG_COLOR
                
                //   nav.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
                //  nav.setViewControllers([vc], animated: true)
                window?.rootViewController = nav
                window?.makeKeyAndVisible()
            }
       
          }
        else{
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            
            //  vc.my_profile = "true"
            
          //  navigation?.setViewControllers([vc], animated: true)
            let nav = UINavigationController.init(rootViewController: vc)
            //   nav.navigationBar.barTintColor = NAV_BG_COLOR
            nav.navigationBar.tintColor = UIColor.white
            //   nav.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
            //  nav.setViewControllers([vc], animated: true)
            window?.rootViewController = nav
            window?.makeKeyAndVisible()
        }
        
    }
    
    
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        
        if LoginFrom == "Gmail"
        {
            let openURLContext = Array(URLContexts).first
            if openURLContext != nil {
                if let URL = openURLContext?.url, let annotation = openURLContext?.options.annotation {
                    GIDSignIn.sharedInstance()?.handle(URL)
                    
                }
            }
        }
        else if LoginFrom == "Facebook"
            
        {
            //            let openURLContext = Array(URLContexts).first
            //            if openURLContext != nil {
            //                if let URL = openURLContext?.url, let annotation = openURLContext?.options.annotation {
            //
            //                    ApplicationDelegate.shared.application(UIApplication.shared, open: URL, options: [UIApplication.OpenURLOptionsKey : Any]())
            //
            
            guard let url = URLContexts.first?.url else {
                return
            }
            let _ = ApplicationDelegate.shared.application(
                UIApplication.shared,
                open: url,
                sourceApplication: nil,
                annotation: [UIApplication.OpenURLOptionsKey.annotation])
            //  ApplicationDelegate.shared.application(UIApplication.shared, open: URL, sourceApplication: openURLContext?.options.sourceApplication, annotation: annotation)
            
            // ApplicationDelegate.shared.application(UIApplication.shared, open: URL, sourceApplication: openURLContext?.options.sourceApplication, annotation: annotation)
            // }
            //}
        }
        // Add any custom logic here.
    }
    
    @available(iOS 13.0, *)
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    @available(iOS 13.0, *)
    func sceneDidBecomeActive(_ scene: UIScene) {
        
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    @available(iOS 13.0, *)
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    @available(iOS 13.0, *)
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    @available(iOS 13.0, *)
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
}


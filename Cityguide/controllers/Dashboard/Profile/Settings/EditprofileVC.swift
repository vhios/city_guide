//
//  EditprofileVC.swift
//  Cityguide
//
//  Created by DECODER on 07/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class EditprofileVC: UIViewController,UITextFieldDelegate,selectAddressDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,PickerDelegate {
  
    var genderArray = ["Male", "Female", "Others"]
    
    @IBOutlet weak var cameraimg: UIButton!
    @IBOutlet weak var birthdaytxt: UITextField!
    @IBOutlet weak var gendertxt: UITextField!
    @IBOutlet weak var addresstxt: UITextField!
    @IBOutlet weak var titletxt: UITextField!
    @IBOutlet weak var submitbtn: UIButton!
    @IBOutlet weak var nametxt: UITextField!
    @IBOutlet weak var lastNameTxt: UITextField!

    @IBOutlet weak var profileimg: UIImageView!
    @IBOutlet weak var profileview: UIView!
    
    let imagePicker = UIImagePickerController()
    var ProfileResponceData : [ProfileData]?
    var picker = CustomPicker()
    var SelectedTextField = UITextField()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "Edit Profile", right: false,profile:true,left:true)
        picker = Bundle.main.loadNibNamed("CustomPicker", owner: self, options: nil)?.last as! CustomPicker
        picker.delegate = self
        cameraimg.layer.cornerRadius = cameraimg.frame.height / 2
        cameraimg.layer.borderColor = UIColor.red.cgColor
        cameraimg.layer.borderWidth = 1
      
        submitbtn.layer.cornerRadius = submitbtn.frame.height / 2
        submitbtn.clipsToBounds = true
        
        self.submitbtn.applyGradient(colours: [UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1),UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1)],locations: [0.0,1.0])
        
        profileimg.layer.cornerRadius = profileimg.frame.height / 2
        profileimg.layer.borderWidth = 2
        profileimg.layer.borderColor = UIColor.red.cgColor
            
        imagePicker.delegate = self
        gendertxt.inputView = picker
        birthdaytxt.inputView = picker
        apiGetProfileData()
        self.submitbtn.addTarget(self, action: #selector(btnNextPressed(_:)), for: .touchUpInside)
        self.cameraimg.addTarget(self, action: #selector(btnCameraPressed(_:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    @objc func callMethod()
    {
        print("hello")
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnNextPressed(_ button : UIButton){
        if nametxt.text?.isEmpty == true{
            UIAlertController().Simplealert(withTitle: "Please enter First name", Message: "", presentOn: self)
        }
        else if nametxt.isValid(testStr: (nametxt.text ?? "").trimmingCharacters(in: .whitespaces)) == false{
            UIAlertController().Simplealert(withTitle: "Please enter valid name", Message: "", presentOn: self)
        }
        else if lastNameTxt.text?.isEmpty == true{
            UIAlertController().Simplealert(withTitle: "Please enter last name", Message: "", presentOn: self)
            
        }else if lastNameTxt.isValid(testStr: (lastNameTxt.text ?? "").trimmingCharacters(in: .whitespaces)) == false{
            UIAlertController().Simplealert(withTitle: "Please enter valid last name", Message: "", presentOn: self)
            
        }else{
            apiUpdateProfileData()
        }
    }
    @objc func btnCameraPressed(_ button : UIButton){
        let alertController = UIAlertController.init(title: "SELECT IMAGE", message: "Please select image", preferredStyle: .actionSheet)
               alertController.addAction(UIAlertAction(title: NSLocalizedString("Camera", comment: ""), style: .default) { _ in
                   self.imagePicker.sourceType = .camera
                   self.present(self.imagePicker, animated: true, completion: nil)
               })
               alertController.addAction(UIAlertAction(title: NSLocalizedString("Gallery", comment: ""), style: .default) { _ in
                   self.imagePicker.sourceType = .photoLibrary
                   self.present(self.imagePicker, animated: true, completion: nil)
               })
               alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { _ in
               }))
               
               self.present(alertController, animated: true, completion: nil)
               
    }
    func selectedData(name: String) {
        self.addresstxt.text = name
    }
    
    //Text field delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
         self.SelectedTextField = textField
        if textField == addresstxt{
            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChoosecityVC") as? ChoosecityVC else {return}
            vc.fromController = "profile"
            vc.addressDelegate = self
            self.navigationController?.present(vc, animated: true, completion: nil)
            //self.navigationController?.pushViewController(vc, animated: true)
        }else if textField == gendertxt{
            IQKeyboardManager.shared.enable = false
            IQKeyboardManager.shared.enableAutoToolbar = false
            picker.DatePickerView.isHidden = true
            picker.Picker.isHidden = false
            picker.DataSource = genderArray
            picker.reloadData()
            
        }else if textField == birthdaytxt{
            picker.DatePickerView.isHidden = false
            picker.Picker.isHidden = true
            picker.DatePickerView.maximumDate = NSDate() as Date
            
        }else{
            
        }
    }
    func PickerView(pickerView: UIPickerView, didSelect button: UIButton) {
      
            if SelectedTextField == gendertxt
            {
                gendertxt.text = genderArray[picker.Picker.selectedRow(inComponent: 0)]
                self.view.endEditing(true)
               
            }else if SelectedTextField == birthdaytxt
            {
                let date = picker.DatePickerView.date
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let dateStr = dateFormatter.string(from: date)
                self.birthdaytxt.text = dateStr
                self.view.endEditing(true)
            }
       
      }
    // get image method
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
           let info1 = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
           profileimg.image = info1
         
           dismiss(animated: true, completion: nil)
       }
       
}
extension EditprofileVC{
    func apiGetProfileData(){
        
        let serviceManager = ServiceManager<ProfileData>.init()
        let dic:[String:Any] = ["userid":UserDefaults.standard.value(forKey: "userid") as? String ?? "",
                                "token" : UserDefaults.standard.value(forKey: "token") as? String ?? ""]
        
        print(dic)
        
        let serviceName = "getProfiledata"
        serviceManager.ServiceName = serviceName
        serviceManager.Parameters = dic
        serviceManager.MakeServiceCall(httpMethod: .get) { (UserResponse) in
            print(UserResponse)
            
            let userResponse = UserResponse as? ResponseModel<ProfileData>
            self.ProfileResponceData = userResponse?.Data
            
            self.addresstxt.text = self.ProfileResponceData?[0].placeForLive
            self.nametxt.text = (self.ProfileResponceData?[0].firstName ?? "")
            self.lastNameTxt.text = (self.ProfileResponceData?[0].lastName ?? "")
            self.titletxt.text = self.ProfileResponceData?[0].username
            self.birthdaytxt.text = self.ProfileResponceData?[0].dob
            self.gendertxt.text =  self.ProfileResponceData?[0].gender
            
           
        }
    }
    //http://work-demo.xyz/city_guide/service/service_v2/updateProfile?userid=37&token=CTGID20200205121458374819&first_name=maitree&last_name=thakker&about_me=IOS%20developer&place_for_live=ahmedabad&gender=female&dob=1995-10-15
    
    func apiUpdateProfileData(){
          
          let serviceManager = ServiceManager<ProfileData>.init()
        let dic:[String:Any] = ["userid":UserDefaults.standard.value(forKey: "userid") as? String ?? "",
                                "token" : UserDefaults.standard.value(forKey: "token") as? String ?? "",
                                "first_name": nametxt.text ?? "",
                                "last_name" : lastNameTxt.text ?? "",
                                "about_me" : titletxt.text ?? "",
                                "place_for_live" : addresstxt.text ?? "",
                                "gender" : gendertxt.text ?? "",
                                "dob" : birthdaytxt.text ?? ""
        ]
          
          print(dic)
          
          let serviceName = "updateProfile"
          serviceManager.ServiceName = serviceName
          serviceManager.Parameters = dic
          serviceManager.MakeServiceCall(httpMethod: .get) { (UserResponse) in
              print(UserResponse)
              
              let userResponse = UserResponse as? ResponseModel<ProfileData>
              self.ProfileResponceData = userResponse?.Data
              
              self.addresstxt.text = self.ProfileResponceData?[0].placeForLive
              self.nametxt.text = (self.ProfileResponceData?[0].firstName ?? "")
              self.lastNameTxt.text = (self.ProfileResponceData?[0].lastName ?? "")

              self.titletxt.text = self.ProfileResponceData?[0].username
              self.birthdaytxt.text = self.ProfileResponceData?[0].dob
              self.gendertxt.text =  self.ProfileResponceData?[0].gender
          }
      }
}


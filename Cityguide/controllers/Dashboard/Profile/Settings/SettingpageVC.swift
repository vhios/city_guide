//
//  SettingpageVC.swift
//  Cityguide
//
//  Created by DECODER on 07/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class SettingpageVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var settingarr = ["Edit profile","Share my profile","Account Settings","About","Help","Terms and condition","Feedback"]
    
    @IBOutlet weak var settingtv: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingtv.delegate = self
        settingtv.dataSource = self
        
        (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "Settings", right: false,profile:true,left:true)
        settingtv.register(UINib(nibName: "Settingcell", bundle: nil), forCellReuseIdentifier: "Settingcell")
        settingtv.register(UINib(nibName: "Logoutcell", bundle: nil), forCellReuseIdentifier: "Logoutcell")
    }
    
    
    @objc func callMethod()
    {
        print("hello")
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return settingarr.count
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
            let cell = settingtv.dequeueReusableCell(withIdentifier: "Settingcell") as! Settingcell
            cell.settingnamelbl.text = settingarr[indexPath.row]
            cell.selectionStyle = .none
            if indexPath.row == 1
            {
                cell.settingimg.image = #imageLiteral(resourceName: "share-1")
            }
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell = settingtv.dequeueReusableCell(withIdentifier: "Logoutcell") as! Logoutcell
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0
        {
            if indexPath.row == 0
            {
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditprofileVC") as? EditprofileVC else {return}
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 1
            {
                if let link = NSURL(string: "https://www.google.com/") {
                let objectsToShare = [link] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                self.present(activityVC, animated: true, completion: nil)
                }
            }
            else if indexPath.row == 2
            {
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "SecondsettingVC") as? SecondsettingVC else {return}
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 3
            {
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutVC") as? AboutVC else {return}
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 4
            {
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "HelpVC") as? HelpVC else {return}
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else if indexPath.row == 5
            { guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "termsandcondi") as? termsandcondi else {return}
                vc.fromController = "true"
                self.navigationController?.pushViewController(vc, animated: true)
            }
                
            else
            {
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackVC") as? FeedbackVC else {return}
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        else
        {
            let alertController = UIAlertController.init(title: "Logout", message: "Are you sure want to logout?.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default) { _ in
                
                UserDefaults.standard.removeObject(forKey: "city")
                UserDefaults.standard.removeObject(forKey: "interest")
                UserDefaults.standard.removeObject(forKey: "userid")
               // UserDefaults.standard.removeObject(forKey: "login")
                UserDefaults.standard.removeObject(forKey: "profile_pic")
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else {return}
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
               
               print("Logout")
            })
            alertController.addAction(UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .default) { _ in
                alertController.dismiss(animated: true, completion: nil)
            })
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { _ in
               
            })
            self.present(alertController, animated: true, completion: nil)
                         
        }
    }
}

//
//  AccountsettingVC.swift
//  Cityguide
//
//  Created by DECODER on 07/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class AccountsettingVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

  @IBOutlet weak var accountTableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

         (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "Account Settings", right: false,profile:true,left:true)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = accountTableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? accountSettingCell
        GlobalViewShadow.ShadowWithBorder(cell!.viewaddEmail, color: .lightGray)
        GlobalViewShadow.ShadowWithBorder(cell!.viewprimaryEmail, color: .lightGray)
        cell?.btnAddEmail.applyGradient(colours: [UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1),UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1)],locations: [0.0,1.0])
        cell?.btnAddEmail.layer.cornerRadius = (cell?.btnAddEmail.frame.height ?? 0) / 2
        cell?.btnAddEmail.clipsToBounds = true
        
        if indexPath.row == 0{
            cell?.btnAddEmail.setTitle("Add Email", for: .normal)
            cell?.lbladdEmail.text = "Add email"
            cell?.txtaddEmail.placeholder = "Add your email id"
          //  cell?.txtprimaryEmail.placeholder = "Required"
            cell?.lblprimaryEmail.text = "Primary Email"
            cell?.lblemailHeader.text = "Email"
            cell?.viewaddEmail.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        }else{
            cell?.btnAddEmail.setTitle("Change Password", for: .normal)
            cell?.lbladdEmail.text = "New"
            cell?.txtaddEmail.placeholder = "Required"
            cell?.txtprimaryEmail.placeholder = "Required"
            cell?.lblprimaryEmail.text = "Current"
            cell?.lblemailHeader.text = "Password"
            cell?.viewprimaryEmail.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
            cell?.viewaddEmail.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        }
        return cell!
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }

}

class accountSettingCell : UITableViewCell{
    @IBOutlet weak var btnAddEmail: UIButton!
    @IBOutlet weak var lblemailHeader: UILabel!
    @IBOutlet weak var lblprimaryEmail: UILabel!
    @IBOutlet weak var lbladdEmail: UILabel!
    @IBOutlet weak var txtprimaryEmail: UITextField!
    @IBOutlet weak var txtaddEmail: UITextField!
    
    @IBOutlet weak var viewprimaryEmail: UIView!
    @IBOutlet weak var viewaddEmail: UIView!

    
}

//
//  FeedbackVC.swift
//  Cityguide
//
//  Created by Maitree on 14/02/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit

class FeedbackVC: UIViewController,UITextViewDelegate {

    @IBOutlet weak var submitbtn: UIButton!
    @IBOutlet weak var feedbackTextView: UITextView!
    @IBOutlet weak var lblFeedback: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "Feedback", right: false,profile:true,left:true)
        
        submitbtn.layer.cornerRadius = submitbtn.frame.height / 2
        submitbtn.clipsToBounds = true
        
        feedbackTextView.layer.cornerRadius = 15
        feedbackTextView.clipsToBounds = true
               
        feedbackTextView.textContainerInset = UIEdgeInsets(top: 20, left: 9, bottom: 10, right: 10)
        
        self.submitbtn.applyGradient(colours: [UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1),UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1)],locations: [0.0,1.0])
        // Do any additional setup after loading the view.
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
//if feedbackTextView.text.count == 0{
            self.lblFeedback.isHidden = true
       // }else{
//self.lblFeedback.isHidden = false
       // }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (textView.text)?.count == 0{
            self.lblFeedback.isHidden = false
        }else{
            self.lblFeedback.isHidden = true
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
         self.lblFeedback.isHidden = true
    }

}

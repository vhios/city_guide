//
//  LAunguageVC.swift
//  Cityguide
//
//  Created by Maitree on 14/02/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit

class LaunguageVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var LaunguageTableView: UITableView!
    var LaungaugeArr = ["ENGLISH","TURKISH","GERMAN","RUSSIAN","SPANISH","FRENCH"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        LaunguageTableView.register(UINib(nibName: "emailnotificationCell", bundle: nil), forCellReuseIdentifier: "emailnotificationCell")
        LaunguageTableView.tableFooterView = UIView()
        (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "Launguages", right: false,profile:true,left:true)
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LaungaugeArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = LaunguageTableView.dequeueReusableCell(withIdentifier: "emailnotificationCell") as! emailnotificationCell
        cell.notificationlbl.text = LaungaugeArr[indexPath.row]
        cell.onOffSwitch.isHidden = true
        GlobalViewShadow.ShadowWithBorder(cell.contentView, color: .lightGray)
        return cell
    }
       

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

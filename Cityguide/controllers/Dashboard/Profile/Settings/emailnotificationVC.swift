//
//  email&notificationVC.swift
//  Cityguide
//
//  Created by DECODER on 07/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class emailnotificationVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
    var notification = ["Notifications", "Email Notifications"]
    
    @IBOutlet weak var emailtb: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailtb.register(UINib(nibName: "emailnotificationCell", bundle: nil), forCellReuseIdentifier: "emailnotificationCell")

        emailtb.delegate = self
        emailtb.dataSource = self
        emailtb.tableFooterView = UIView()
       (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "Email & Notifications", right: false,profile:true,left:true)

        // Do any additional setup after loading the view.
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notification.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = emailtb.dequeueReusableCell(withIdentifier: "emailnotificationCell") as! emailnotificationCell
        cell.notificationlbl.text = notification[indexPath.row]
        cell.onOffSwitch.tag = indexPath.row
        cell.onOffSwitch.addTarget(self, action: #selector(buttonClicked(_:)), for: .valueChanged)
          return cell
      }
    
    @objc func buttonClicked(_ sender: UISwitch) {
        let indexpath = NSIndexPath.init(row: sender.tag, section: 0)
        let cell = emailtb.cellForRow(at: indexpath as IndexPath) as? emailnotificationCell
        if (cell?.onOffSwitch.isOn)! {
            //textLabel.text = "The Switch is Off"
            cell?.onOffSwitch.isOn = false
          // cell?.onOffSwitch.setOn(false, animated:true)
        } else {
            cell?.onOffSwitch.isOn = true
            //cell?.onOffSwitch.setOn(true, animated:true)
        }
    }
}

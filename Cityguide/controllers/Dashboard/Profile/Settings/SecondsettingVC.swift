//
//  SecondsettingVC.swift
//  Cityguide
//
//  Created by DECODER on 07/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class SecondsettingVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
  
    var setting = ["Accounts","Email&Notifications","Languages"]
//    var settingimag = #imageLiteral(resourceName: <#T##String#>)
    @IBOutlet weak var settingtv: UITableView!
    
    @IBOutlet weak var heightoftable: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "Account Settings", right: false,profile:true,left:true)
        settingtv.delegate = self
        settingtv.dataSource = self
        settingtv.register(UINib(nibName: "Settingcell", bundle: nil), forCellReuseIdentifier: "Settingcell")
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return setting.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = settingtv.dequeueReusableCell(withIdentifier: "Settingcell") as! Settingcell
        cell.settingnamelbl.text = setting[indexPath.row]
        return cell
      }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccountsettingVC") as? AccountsettingVC else {return}
        self.navigationController?.pushViewController(vc, animated: true)
        }
        else if  indexPath.row == 1
        {
            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "emailnotificationVC") as? emailnotificationVC else {return}
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if  indexPath.row == 2
        {
            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LaunguageVC") as? LaunguageVC else {return}
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

}

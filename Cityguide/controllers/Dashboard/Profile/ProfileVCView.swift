//
//  ProfileVCView.swift
//  Cityguide
//
//  Created by DECODER on 11/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class ProfileVCView: UIViewController,UITableViewDelegate,UITableViewDataSource {

     @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var ImageProfile: UIImageView!
    @IBOutlet weak var FeedTableView: UITableView!
    @IBOutlet weak var lblFollowing: UILabel!
    @IBOutlet weak var lblFollowers: UILabel!
    @IBOutlet weak var addPin: UIImageView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnFollowers: UIButton!
    @IBOutlet weak var btnFollowing: UIButton!
    @IBOutlet weak var lblProfileName: UILabel!
    var ProfileResponceData : [ProfileData]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 250, height: 20))
        searchBar.placeholder = "Search"
        searchBar.searchTextField.borderStyle = .none
        searchBar.searchTextField.backgroundColor = .white
        searchBar.searchTextField.layer.cornerRadius =  15
        searchBar.searchTextField.clipsToBounds = true
               //  searchBar.backgroundColor = UIColor.white
        var leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        
        ImageProfile.layer.cornerRadius = ImageProfile.frame.size.height / 2
        ImageProfile.clipsToBounds = true
        ImageProfile.layer.borderColor = UIColor.red.cgColor
        ImageProfile.layer.borderWidth = 1
        
       // FeedTableView.register(UINib(nibName: "Interestprofilecell", bundle: nil), forCellReuseIdentifier: "Interestprofilecell")
        FeedTableView.tableHeaderView = headerView
        self.btnMenu.addTarget(self, action: #selector(btnMorePRessed(_:)), for: .touchUpInside)
         FeedTableView.register(UINib(nibName: "feedCell", bundle: nil), forCellReuseIdentifier: "feedCell")
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        apiGetProfileData()
        (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "", right: true,profile:true)
    }
    
    @IBAction func btnFollowing(_ sender: UIButton) {
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowersVC") as? FollowersVC else {return}
       
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnFollowers(_ sender: UIButton) {
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowersVC") as? FollowersVC else {return}
         vc.fromController = "true"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnMorePRessed(_ button : UIButton){
        let alertController = UIAlertController.init(title: "SELECT OPTIONS", message: "Please select One", preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Report", comment: ""), style: .default) { _ in
           
        })
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Block", comment: ""), style: .default) { _ in
           
        })
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { _ in
        }))
                     
        self.present(alertController, animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if indexPath.row == 0
//        {
//
//            let cell = FeedTableView.dequeueReusableCell(withIdentifier: "Interestprofilecell") as! Interestprofilecell
//            return cell
//
//        }
//        else
//        {
            let cell = FeedTableView.dequeueReusableCell(withIdentifier: "feedCell") as! feedCell
            cell.inok = indexPath.row
        cell.moreBtn.isHidden = true
            return cell
      // v  }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        
    }

}

extension ProfileVCView{
    func apiGetProfileData(){
        
        let serviceManager = ServiceManager<ProfileData>.init()
        let dic:[String:Any] = ["userid":UserDefaults.standard.value(forKey: "userid") as? String ?? "",
                                "token" : UserDefaults.standard.value(forKey: "token") as? String ?? ""]
        
        print(dic)
        
        let serviceName = "getProfiledata"
        serviceManager.ServiceName = serviceName
        serviceManager.Parameters = dic
        serviceManager.MakeServiceCall(httpMethod: .get) { (UserResponse) in
            print(UserResponse)
            
            let userResponse = UserResponse as? ResponseModel<ProfileData>
            self.ProfileResponceData = userResponse?.Data
            if self.ProfileResponceData?.isEmpty == true{
                
            }else{
                self.lblAddress.text = self.ProfileResponceData?[0].placeForLive
                self.lblProfileName.text = (self.ProfileResponceData?[0].firstName ?? "") + " " + (self.ProfileResponceData?[0].lastName ?? "")
            }
            
          
        }
    }
}


//
//  FollowersVC.swift
//  Cityguide
//
//  Created by Maitree on 14/02/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit

class FollowersVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var fromController : String!
    @IBOutlet weak var FollowersTableview: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        if fromController == "true"{
            (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "Followers", right: false,profile:true,left:true)
        }else{
             (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "Following", right: false,profile:true,left:true)
        }
        
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return 10
       }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = FollowersTableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? followersCell
        cell?.FollowBtn.layer.borderColor = UIColor(red: 231/255, green: 59/255, blue: 61/255, alpha: 1).cgColor
        cell?.FollowBtn.layer.borderWidth = 1
        cell?.FollowBtn.layer.cornerRadius = (cell?.FollowBtn.frame.size.height ?? 0) / 2
        cell?.FollowBtn.clipsToBounds = true
        
        cell?.profileimg.layer.cornerRadius = (cell?.profileimg.frame.size.height ?? 0) / 2
        cell?.profileimg.clipsToBounds = true
        
        if fromController == "true"{
            cell?.FollowBtn.setTitle("Follow", for: .normal)
            cell?.MoreBtn.isHidden = false
            cell?.MoreBtn.addTarget(self, action: #selector(btnMorePRessed(_:)), for: .touchUpInside)
        }else{
            cell?.FollowBtn.setTitle("Unfollow", for: .normal)
            cell?.MoreBtn.isHidden = true
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    @objc func btnMorePRessed(_ button : UIButton){
           let alertController = UIAlertController.init(title: "SELECT OPTIONS", message: "Please select One", preferredStyle: .actionSheet)
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Block", comment: ""), style: .default) { _ in
            
            })
           alertController.addAction(UIAlertAction(title: NSLocalizedString("Unfollow", comment: ""), style: .default) { _ in
              
           })
          
           alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { _ in
           }))
                        
           self.present(alertController, animated: true, completion: nil)
       }

}
class followersCell : UITableViewCell{
    @IBOutlet weak var FollowBtn: UIButton!
    @IBOutlet weak var MoreBtn: UIButton!
    @IBOutlet weak var nametxt: UILabel!
    @IBOutlet weak var userNameTxt: UILabel!
    @IBOutlet weak var profileimg: UIImageView!
}

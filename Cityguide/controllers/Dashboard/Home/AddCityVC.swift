//
//  AddCityVC.swift
//  Cityguide
//
//  Created by Maitree on 12/02/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class AddCityVC: UIViewController,UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate,GMSMapViewDelegate {
    
    @IBOutlet weak var searchingtxt: UITextField!
    @IBOutlet weak var searchingtb: UITableView!
    @IBOutlet weak var searchingview: UIView!
    @IBOutlet weak var tblCityView: UIView!
    @IBOutlet var chooseview: UIView!
    @IBOutlet weak var lblValidation: UILabel!
    @IBOutlet weak var citynameTb: UITableView!

    @IBOutlet weak var heightConstasnt: NSLayoutConstraint!
    @IBOutlet weak var viewheightConstasnt: NSLayoutConstraint!
    @IBOutlet weak var tableviewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableviewTopHeight: NSLayoutConstraint!
    @IBOutlet weak var closeButton: UIButton!
    
    var searchCityData : [cityData]?
    var cityList : [addCity]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
       citynameTb.layer.cornerRadius = 20
       citynameTb.clipsToBounds = true
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.isNavigationBarHidden = false
        
        self.chooseview.applyGradient1(colours: [UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1),UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1)],locations: [0.0,1.0])
        
        GlobalViewShadow.ShadowWithBorder(tblCityView, color: UIColor.darkGray)
        GlobalViewShadow.ShadowWithBorder(citynameTb, color: UIColor.darkGray)
        
        searchingtb.estimatedRowHeight = 44
        searchingtb.rowHeight = UITableView.automaticDimension
        
        lblValidation.isHidden = true
        searchingtb.tableFooterView = UIView()
        let leftView = UIView(frame: CGRect(x: 00.0, y: 0.0, width: 20.0, height: 2.0))
        searchingtxt.leftView = leftView
        searchingtxt.leftViewMode = .always
        
        searchingtb.register(UINib(nibName: "Citycell", bundle: nil), forCellReuseIdentifier: "Citycell")
        citynameTb.register(UINib(nibName: "Citycell", bundle: nil), forCellReuseIdentifier: "Citycell")
        searchingtb.delegate = self
        searchingtb.dataSource = self
        
        searchingtxt.layer.cornerRadius = searchingtxt.frame.size.height / 2
        searchingtxt.clipsToBounds = true
        
        // searchingtb.frame = CGRect(x: searchingtb.frame.origin.x, y: searchingtb.frame.origin.y, width: searchingtb.frame.size.width, height: searchingtb.contentSize.height)
        searchingtxt.layer.borderWidth = 0
        closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        apiGetCityListData()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "", right: false,profile:false,left:true)
     
    }

    
    @objc func closeButtonPressed(){
        self.searchingtxt.text = ""
        self.citynameTb.isHidden = true
        //   lblValidation.isHidden = false
        lblValidation.hideWithAnimation(hidden: false)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == citynameTb{
            if searchCityData?.count == nil{
                return 0
            }else{
                return searchCityData?.count ?? 0
            }
        }else{
            if cityList?.count == nil{
                return 0
            }else{
                return cityList?.count ?? 0
            }
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == citynameTb{
            let cell = citynameTb.dequeueReusableCell(withIdentifier: "Citycell") as! Citycell
            cell.citynamelbl.text = (searchCityData?[indexPath.row].allName ?? "")
            cell.closeBtn.isHidden = true
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = searchingtb.dequeueReusableCell(withIdentifier: "Citycell") as! Citycell
            cell.citynamelbl.text = (cityList?[indexPath.row].cityName ?? "")
            cell.closeBtn.isHidden = false
            cell.closeBtn.addTarget(self, action: #selector(btnRemoveCity(_:)), for: .touchUpInside)
            cell.closeBtn.tag = indexPath.row
            cell.closeBtn.setImage(#imageLiteral(resourceName: "cancel-2"), for: .normal)
            cell.selectionStyle = .none
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == citynameTb{
            print(searchCityData?[indexPath.row].allName ?? "")
            UserDefaults.standard.set(searchCityData?[indexPath.row].allName ?? "", forKey: "city")
            if self.cityList?.count ?? 0 > 5{
                UIAlertController().Simplealert(withTitle: "You can add maximum 5 cities. if you want to add another please remove city from the given list.", Message: "", presentOn: self)
                self.citynameTb.isHidden = true
            }else{
                apiAddCity(stateid: searchCityData?[indexPath.row].stateID ?? "", city_id: searchCityData?[indexPath.row].id ?? "", country_id: searchCityData?[indexPath.row].countryID ?? "", name: searchCityData?[indexPath.row].name ?? "")
            }
           
        }else{
           
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print((textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? "")
        if ((textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? "").count >= 3 {
            
            //  if textField.text?.count ?? 0 >= 2{
            DispatchQueue.main.async {
                self.apiGetSerachCityData(search: ((textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? "").trimmingCharacters(in: .whitespaces))
            }
            lblValidation.hideWithAnimation(hidden: true)
            // lblValidation.isHidden = true
        }else{
            //   lblValidation.isHidden = false
            lblValidation.hideWithAnimation(hidden: false)
            self.citynameTb.isHidden = true
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        if textField.text?.count ?? 0 >= 3{
         //   apiGetSerachCityData(search: (textField.text ?? "").trimmingCharacters(in: .whitespaces))
        }else{
            self.citynameTb.isHidden = true
            // apiGetSerachCityData(search: textField.text ?? "")
            //  lblValidation.isHidden = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        apiGetSerachCityData(search: (textField.text ?? "").trimmingCharacters(in: .whitespaces))
        return true
    }
   //MARK:- tableview btn action
    @objc func btnRemoveCity(_ button : UIButton){
        
        apiRemoveCityData(cityId: cityList?[button.tag].cityID ?? "")
        
    }
}
extension AddCityVC{
    //MARK:-  cityData all city list
    func apiGetSerachCityData(search : String){
        
        let serviceManager = ServiceManager<cityData>.init()
        let dic:[String:Any] = ["city":search,
                                "token" : UserDefaults.standard.value(forKey: "token") as? String ?? ""]
        print(dic)
        
        let serviceName = "userSearchcity"
        serviceManager.ServiceName = serviceName
        serviceManager.Parameters = dic
        serviceManager.ShowLoader = false
        serviceManager.MakeServiceCall(httpMethod: .get) { (UserResponse) in
            print(UserResponse)
            self.tblCityView.isHidden = false
            let userResponse = UserResponse as? ResponseModel<cityData>
            self.searchCityData = userResponse?.Data
            self.citynameTb.isHidden = false
            
            self.citynameTb.reloadData()
           // self.tableviewHeight.constant = 200
            self.tableviewHeight.constant = self.citynameTb.contentSize.height
        }
    }
    
    // MARK:-  add city
    func apiAddCity(stateid:String,city_id:String,country_id:String,name:String){
        
        let serviceManager = ServiceManager<addCity>.init()
        let dic:[String:Any] = ["state_id":stateid,
                                "city_id":city_id,
                                "country_id":country_id,
                                "user_id":UserDefaults.standard.value(forKey: "userid") as? String ?? "",
                                "name":name,
                                "token":UserDefaults.standard.value(forKey: "token") as? String ?? ""]
        
        print(dic)
        let serviceName = "userAddCity"
        serviceManager.ServiceName = serviceName
        serviceManager.Parameters = dic
        serviceManager.ShowLoader = true
        
        serviceManager.MakeServiceCall(httpMethod: .get) { (UserResponse) in
            print(UserResponse)
            self.citynameTb.isHidden = true
            self.tableviewHeight.constant = 0
            self.apiGetCityListData()
        }
    }
    //MARK:-  citylist by userid
        func apiGetCityListData(){
              
              let serviceManager = ServiceManager<addCity>.init()
              let dic:[String:Any] = ["user_id":UserDefaults.standard.value(forKey: "userid") as? String ?? "",
                                      "token" : UserDefaults.standard.value(forKey: "token") as? String ?? ""]
              print(dic)
              
              let serviceName = "userCityList"
              serviceManager.ServiceName = serviceName
              serviceManager.Parameters = dic
            //  serviceManager.ShowLoader = false
              serviceManager.MakeServiceCall(httpMethod: .get) { (UserResponse) in
                  print(UserResponse)
                  
                  let userResponse = UserResponse as? ResponseModel<addCity>
                  self.cityList = userResponse?.Data
                
                if self.cityList?.count == 0{
                    self.tblCityView.isHidden = true

                }else{
                    self.tblCityView.isHidden = false
                    self.searchingtb.isHidden = false
                    self.searchingtb.reloadData()
                    // self.viewheightConstasnt.constant = self.searchingtb.contentSize.height
                    self.heightConstasnt.constant = self.searchingtb.contentSize.height
                }
                 
              }
          }
    
    //http://work-demo.xyz/city_guide/service/service_v2/userCityDelete?token=CTGID2020021002225839922&user_id=2&city_id=783
    
    //MARK:-  citylist by userid
    func apiRemoveCityData(cityId:String){
              
              let serviceManager = ServiceManager<ResponseModelSuccess>.init()
              let dic:[String:Any] = ["user_id":UserDefaults.standard.value(forKey: "userid") as? String ?? "",
                                      "token" : UserDefaults.standard.value(forKey: "token") as? String ?? "",
                                      "city_id":cityId
        ]
              print(dic)
              
              let serviceName = "userCityDelete"
              serviceManager.ServiceName = serviceName
              serviceManager.Parameters = dic
            
              serviceManager.MakeServiceCall(httpMethod: .get) { (UserResponse) in
                  print(UserResponse)
                  self.tblCityView.isHidden = false
                  let userResponse = UserResponse as? ResponseModel<ResponseModelSuccess>
                  self.apiGetCityListData()
              }
          }
    
}



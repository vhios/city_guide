//
//  DashboardVC.swift
//  Cityguide
//
//  Created by DECODER on 11/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class DashboardVC: UIViewController {

    @IBOutlet weak var peoplelbl: UILabel!
    @IBOutlet weak var interestlbl: UILabel!
    @IBOutlet weak var peoplebtn: UIButton!
    @IBOutlet weak var interestbtn: UIButton!
    @IBOutlet weak var DashboardInterestview: UIView!
    @IBOutlet weak var interestVC: UIView!
    
    @IBOutlet weak var peoplevc: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "", right: true)
        
        self.navigationItem.hidesBackButton = true
        
        var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
        searchBar.placeholder = "Search"
        searchBar.searchTextField.borderStyle = .none
        searchBar.searchTextField.backgroundColor = .white
        searchBar.searchTextField.layer.cornerRadius = 15
        searchBar.searchTextField.clipsToBounds = true
        var leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        
        let notificationbtn = UIButton(type: .custom)
        notificationbtn.setImage(#imageLiteral(resourceName: "notification"), for: .normal)
        notificationbtn.addTarget(self, action:#selector(callMethod), for: .touchUpInside)
        notificationbtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let barButtons = UIBarButtonItem(customView: notificationbtn)
        
        let addcity = UIButton(type: .custom)
        addcity.setImage(#imageLiteral(resourceName: "plus-positive-add-mathematical-symbol"), for: .normal)
        addcity.setTitle("  Add city", for: .normal)
        addcity.addTarget(self, action:#selector(notify), for: .touchUpInside)
        addcity.layer.backgroundColor = UIColor.white.cgColor
        addcity.setTitleColor(UIColor.red, for: .normal)
        addcity.titleLabel?.font = UIFont(name: "Montserrat", size: 12)
        addcity.frame = CGRect(x: 0, y: 0, width: 90, height: 30)
        addcity.layer.cornerRadius = addcity.frame.height / 2
        addcity.clipsToBounds = true
        let barButtons1 = UIBarButtonItem(customView: addcity)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItems = [barButtons,barButtons1]
        
    //  self.navigationController?.navigationBar.barTintColor = UIColor.red

    DashboardInterestview.applyGradient(colours: [UIColor(red: 231/255, green: 59/255, blue: 61/255, alpha: 1), UIColor(red: 241/255, green: 91/255, blue: 93/255, alpha: 1)])
        
     //   peoplebtn.applyGradient(colours: [UIColor(red: 231/255, green: 59/255, blue: 61/255, alpha: 1), UIColor(red: 241/255, green: 91/255, blue: 93/255, alpha: 1)])
        
       // interestbtn.applyGradient(colours: [UIColor(red: 231/255, green: 59/255, blue: 61/255, alpha: 1), UIColor(red: 241/255, green: 91/255, blue: 93/255, alpha: 1)])
        
        interestVC.isHidden = false
        peoplevc.isHidden = true
        interestlbl.isHidden = false
        peoplelbl.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
         //self.navigationController?.navigationBar.barTintColor = UIColor.red
    }
    
      @objc func callMethod()
        {
//           guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else {return}
//               self.hidesBottomBarWhenPushed = true
//            self.navigationController?.pushViewController(vc, animated: true)
        }

    @objc func notify()
      {
            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddCityVC") as? AddCityVC else {return}
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
      }
}

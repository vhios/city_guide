//
//  mytabbar.swift
//  Cityguide
//
//  Created by DECODER on 11/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class mytabbar: UITabBarController {
    
    @IBOutlet weak var myTabBar: UITabBar?


    override func viewDidLoad() {
        super.viewDidLoad()
        //myTabBar?.tintColor = UIColor.white
           tabBarItem.title = ""
           setTabBarItems()
        // Do any additional setup after loading the view.
    }
    
    func setTabBarItems(){
        let myTabBarItem1 = (self.tabBar.items?[0])! as UITabBarItem
        myTabBarItem1.title = "Home"
        myTabBarItem1.image = #imageLiteral(resourceName: "home1").withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem1.selectedImage = #imageLiteral(resourceName: "home").withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
   //     myTabBarItem1.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)

         let myTabBarItem2 = (self.tabBar.items?[1])! as UITabBarItem
         myTabBarItem2.image = #imageLiteral(resourceName: "ask").withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
         myTabBarItem2.selectedImage = #imageLiteral(resourceName: "ask1").withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        // myTabBarItem2.title = "Ask"
        // myTabBarItem2.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)

        let myTabBarItem3 = (self.tabBar.items?[2])! as UITabBarItem
        myTabBarItem3.image = #imageLiteral(resourceName: "profile").withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        myTabBarItem3.selectedImage = #imageLiteral(resourceName: "profile1").withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
     //   myTabBarItem3.title = "Profile"
     //   myTabBarItem3.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
    }

}

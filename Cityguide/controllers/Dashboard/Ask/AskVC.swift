//
//  AskVC.swift
//  Cityguide
//
//  Created by Maitree on 17/02/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit

class AskVC: UIViewController,UITextViewDelegate {
    
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var topStack: UIStackView!
    @IBOutlet weak var imgprofile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnPublic: UIButton!
    @IBOutlet weak var txtQuestion: UITextView!
    @IBOutlet weak var lblPlaceHolder: UILabel!
    @IBOutlet weak var btnAddQuestion: UIButton!
    @IBOutlet weak var btncancel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnPublic.layer.cornerRadius = btnPublic.frame.height / 2
        btnPublic.clipsToBounds = true
        
        btnAddQuestion.layer.cornerRadius = btnAddQuestion.frame.height / 2
        btnAddQuestion.clipsToBounds = true
        
        btncancel.layer.cornerRadius = btncancel.frame.height / 2
        btncancel.clipsToBounds = true
        
        borderView.layer.borderWidth = 0.3
        borderView.layer.borderColor = UIColor.lightGray.cgColor
        
        imgprofile.layer.cornerRadius = imgprofile.frame.height / 2
        imgprofile.clipsToBounds = true
        imgprofile.layer.borderWidth = 0.8
        imgprofile.layer.borderColor = UIColor.red.cgColor
        
        txtQuestion.delegate = self
        
        self.btnAddQuestion.applyGradient(colours: [UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1),UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1)],locations: [0.0,1.0])
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
       (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "Questions", right: false,profile:true,left:false)
        
        let notificationbtn = UIButton(type: .custom)
        let origImage = #imageLiteral(resourceName: "cancel")
        let tintedImage = origImage.withRenderingMode(.alwaysTemplate)
        notificationbtn.setImage(tintedImage, for: .normal)
        notificationbtn.tintColor = UIColor.white
        
        //  notificationbtn.addTarget(self, action:#selector(callMethod), for: .touchUpInside)
        
        notificationbtn.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        let leftNavBarButton = UIBarButtonItem(customView:notificationbtn)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        
    }
    
    //MARK:- UItextField delegate methods
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let text = (textView.text as NSString).replacingCharacters(in: range, with: text)
        
        if text.count > 0 {
            lblPlaceHolder.isHidden = true
        }else{
            lblPlaceHolder.isHidden = false
        }
        return true
    }
    
}

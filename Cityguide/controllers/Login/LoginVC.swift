//
//  LoginVC.swift
//  CityGuideApp
//
//  Created by DECODER on 06/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
import AuthenticationServices

class LoginVC: UIViewController ,ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding,GIDSignInDelegate{
    
    @IBOutlet weak var txtEmail:UITextField!
   
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnForgotPswd: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnFbLogin: UIButton!
    @IBOutlet weak var btnGoogleLogin: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var viewLoginDetails: UIView!
    @IBOutlet weak var btnViewPswd: UIButton!
    @IBOutlet weak var btnAppleLogin: UIButton!

    var fromController : String?
    var attributedString = NSMutableAttributedString(string:"")

 //   var Login:[LoginData]?
    var socialLoogin : [socialLoginResponse]?

    override func viewDidLoad() {
        super.viewDidLoad()
        //                self.tabBarItem.accessibilityElementsHidden = true
        
        txtPassword.isSecureTextEntry = true
        viewLoginDetails.layer.cornerRadius = 12
        viewLoginDetails.clipsToBounds = true
       // self.navigationController?.setupNavBack(onVC: self, withTitle: "", right: false)
      //  mynavigation().setupNavBack(onVC: self, withTitle: "", right: false)

        txtEmail.text = "maitree@virtualheight.com"
        txtPassword.text = "Maitree@123"

        
        self.navigationController?.isNavigationBarHidden = true
//        txtEmail.layer.cornerRadius = txtEmail.frame.height / 2.2
//        txtEmail.layer.borderColor = UIColor.darkGray.cgColor
//        txtEmail.layer.borderWidth = 1.0
//        txtEmail.clipsToBounds = true
//
//        txtPassword.layer.cornerRadius = txtPassword.frame.height / 2.2
//        txtPassword.clipsToBounds = true
        
        btnLogin.layer.cornerRadius = btnLogin.frame.height / 2.2
        btnLogin.clipsToBounds = true
        self.hidesBottomBarWhenPushed = true
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.delegate = self

        let attributedString = NSAttributedString(string: NSLocalizedString("Register Now ->", comment: ""), attributes:[
            NSAttributedString.Key.font : UIFont.init(name: "Montserrat-SemiBold", size: 15)!,
            NSAttributedString.Key.foregroundColor : UIColor.red,
            NSAttributedString.Key.underlineStyle:1.0
        ])
        
        btnRegister.setAttributedTitle(attributedString, for: .normal)
        
//        var attrs : [NSAttributedString.Key : Any] = [
//        NSAttributedString.Key.font : UIFont.init(name: "Montserrat-Bold", size: 15) as Any,
//        NSAttributedString.Key.foregroundColor : UIColor.red,
//        NSAttributedString.Key.underlineStyle : 1]
//        let buttonTitleStr = NSMutableAttributedString(string:"Register Now ->", attributes:attrs)
//        attributedString.append(buttonTitleStr)
//        btnRegister.setAttributedTitle(attributedString, for: .normal)
        
        if #available(iOS 13.0, *){
            self.btnAppleLogin.isHidden = false
        }else{
            self.btnAppleLogin.isHidden = true
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.btnLogin.applyGradient(colours: [UIColor(red: 231/255, green: 59/255, blue: 61/255, alpha: 1), UIColor(red: 241/255, green: 91/255, blue: 93/255, alpha: 1)])
        //        self.view.applyGradient(colours: [UIColor.white], locations: [0.0, 0.5, 1.0])
        btnLogin.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        dropShadow(color: .black, opacity: 0.2, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        self.hidesBottomBarWhenPushed = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // self.navigationController?.navigationBar.isTranslucent = true
        //self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
      //MARK:- for shadow
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        viewLoginDetails.layer.masksToBounds = false
        viewLoginDetails.layer.shadowColor = color.cgColor
        viewLoginDetails.layer.shadowOpacity = opacity
        viewLoginDetails.layer.shadowOffset = offSet
        viewLoginDetails.layer.shadowRadius = radius
        viewLoginDetails.layer.shadowPath = UIBezierPath(rect: viewLoginDetails.bounds).cgPath
        viewLoginDetails.layer.shouldRasterize = true
        viewLoginDetails.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
   //MARK:- button actions
    @IBAction func btnForgotPswdPressed(_ sender: UIButton) {
        let push = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        
        //push.fromController = "true"
           //     push.DataToPassRegister = [:]
//                let backItem = UIBarButtonItem()
//                backItem.title = ""
//                navigationItem.backBarButtonItem = backItem // This will show in the
            self.navigationController?.pushViewController(push, animated: true)
        
    }
    
    @IBAction func btnLoginPressed(_ sender: UIButton) {
        if txtEmail.text?.isEmpty == true{
            UIAlertController().Simplealert(withTitle:"Please enter email", Message: "", presentOn: self)
            
            //txtFirstName.setTitleVisible(true, animated: true, animationCompletion: nil)
        }
        else if txtEmail.isValidEmail(txtEmail.text ?? "") == false{
            UIAlertController().Simplealert(withTitle:"Please enter valid email", Message: "", presentOn: self)
            
        }else if txtPassword.text?.isEmpty == true{
            UIAlertController().Simplealert(withTitle:"Please enter password", Message: "", presentOn: self)
            //txtFirstName.setTitleVisible(true, animated: true, animationCompletion: nil)
        }else{
            apiLogin()
        }
        
    }
    
    @IBAction func btnViewPswdPressed(_ sender: UIButton) {
        if(sender.isSelected == true) {
            txtPassword.isSecureTextEntry = false
        } else {
            txtPassword.isSecureTextEntry = true
        }

        sender.isSelected = !sender.isSelected
        
    }
    
    @IBAction func btnFbLoginPressed(_ sender: UIButton) {
        //MARK:- facebook sign in method
        objModelddata.type = "faceebook"
        LoginFrom = "Facebook"
        var disc :[String:AnyObject]!
        let FbloginManager = LoginManager()
        
        DispatchQueue.main.async {
            
            FbloginManager.logIn(permissions: ["email","public_profile"], from: self) { (result, error) in
                
                if error != nil {
                    //  UIAlertController().Simplealert(withTitle: (error?.localizedDescription)!, Message: "", presentOn: self)
                }else{
                    
                    if (result?.grantedPermissions.contains("email")) != nil{
                        
                        if((AccessToken.current) != nil){
                            
                            GraphRequest(graphPath: "ME", parameters:["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connect, result, error) in
                                
                                if error == nil {
                                    
                                    disc = result as? [String : AnyObject]
                                    print(result!)
                                    print(disc)
                                    let lname = disc!["last_name"] as? String ?? ""
                                    let name = disc!["first_name"] as? String ?? ""
                                    let email = disc!["email"] as? String ?? ""
                                    let socialId = disc!["id"] as? String ?? ""
                                    let mobile = disc!["mobile"] as? String ?? ""
                                    
                                    if let picture = disc["picture"] as? NSDictionary , let data = picture["data"] as? NSDictionary , let url = data["url"] as? String {
                                        print(url , "url")
                                        //ShowAlert(title: "Successfully logged in! 👍", msg: "", view: controller)
                                        var imgURL = NSURL(string: url)
                                        
                                        var imageData = NSData(contentsOf: imgURL! as URL)
                                        var image = UIImage(data: imageData! as Data)
                                        
                                        print(name,email,"fullname","ëmail")
                                        
                                        print(image!)
                                        
                                        objModelddata.firstName = name
                                        objModelddata.lastName = lname
                                        objModelddata.email = email
                                        objModelddata.facebookId = socialId

                                        //self.profile_Image = image!
                                        //self.profile_url = image
                                    }
                                    
                                    self.apiCheckSocialLogin(email : email)
                                    //  self.API_register_data(Name: name, email: email, mobile: mobile , social_id: socialId, social_type: "facebook")
                               // FbloginManager.logOut()
                                    
                                }else{
                                    
                                    print(error?.localizedDescription ?? Error.self)
                                }
                            })
                        }
                    }
                }
                
            }
        }
        
    }
    
    @IBAction func btnGoogleLogin(_ sender: UIButton) {
        LoginFrom = "Gmail"
        objModelddata.type = "Gmail"

        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func btnAppleLogin(_ sender: UIButton) {
        appleSignin()
    }
    
    @IBAction func btnRegisterPressed(_ sender: UIButton) {
        let push = self.storyboard?.instantiateViewController(withIdentifier: "RegisterPageController") as! RegisterPageController
        //        push.DataToPassRegister = [:]
        //        let backItem = UIBarButtonItem()
        //        backItem.title = ""
        //        navigationItem.backBarButtonItem = backItem // This will show in the
        self.navigationController?.pushViewController(push, animated: true)
    }
    
    
    // MARK:- google sign in method
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
            var image_url = ""
            let idToken = user.authentication.idToken  // Safe to send to the server
            let fullName = user.profile.name
            let email = user.profile.email
            
            objModelddata.firstName = fullName
            objModelddata.lastName = user.profile.familyName
            objModelddata.email = email
            objModelddata.gogleId = idToken
            
            if user.profile.hasImage {
                let image  = signIn.currentUser.profile.imageURL(withDimension: 120)
                image_url = (image?.absoluteString)!
                var imgURL = NSURL(string: image_url)
                
                var imageData = NSData(contentsOf: imgURL! as URL)
                var image1 = UIImage(data: imageData! as Data)
                
                print(image1!)
                print(fullName!,email!,"fullname","ëmail")
                
                //self.profile_Image = image1!
            }
            GIDSignIn.sharedInstance()?.signOut()
            apiCheckSocialLogin(email : email ?? "")
           
            //  API_register_data(Name: fullName ?? "" , email: email ?? "" , mobile: " " , social_id: idToken ?? "", social_type: "google")
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        print(error.localizedDescription)
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    //MARK:- Apple sign in method
    func appleSignin(){
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            //  authorizationController.authorizationRequests = request
            authorizationController.performRequests()
            
        } else {
            // Fallback on earlier versions
        }
    }
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            // Create an account as per your requirement
            let appleId = appleIDCredential.user
            let identifier = appleIDCredential.identityToken
            let appleUserFirstName = appleIDCredential.fullName?.givenName
            let appleUserLastName = appleIDCredential.fullName?.familyName
            let appleUserEmail = appleIDCredential.email
            
            objModelddata.firstName = appleUserFirstName
            objModelddata.lastName = appleUserLastName
            objModelddata.email = appleUserEmail
            objModelddata.facebookId = appleId
            print("appleid:\(appleId)","name:\(appleUserFirstName)\(appleUserLastName)","email:\(appleUserEmail)")
            apiCheckSocialLogin(email : appleUserEmail ?? "")
            setupAppleIDCredentialObserver(id: appleId)
            //Write your code
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            let appleUsername = passwordCredential.user
            let applePassword = passwordCredential.password
            //Write your code
        }
        
    }
    func setupAppleIDCredentialObserver(id: String) {
        if #available(iOS 13.0, *) {
            let authorizationAppleIDProvider = ASAuthorizationAppleIDProvider()
            
            authorizationAppleIDProvider.getCredentialState(forUserID: id) { (credentialState: ASAuthorizationAppleIDProvider.CredentialState, error: Error?) in
                if let error = error {
                    print(error)
                    // Something went wrong check error state
                    return
                }
                switch (credentialState) {
                case .authorized:
                    //User is authorized to continue using your app
                    break
                case .revoked:
                    //User has revoked access to your app
                    break
                case .notFound:
                    //User is not found, meaning that the user never signed in through Apple ID
                    break
                default: break
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error as? ASAuthorizationError)
        
        // Handle error.
    }
    
    
}

extension UIView {
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 1)
    }
}
extension UIButton
{
    func applyGradientBtn(colors: [CGColor])
    {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.frame = self.frame
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
extension LoginVC{
    //MARK:- apicall for login data
   
    func apiLogin(){
        
        let serviceManager = ServiceManager<LoginData>.init()
        let dic:[String:Any] = ["email":txtEmail.text ?? "",
                                "password":txtPassword.text ?? "",
                                "device_id" : objModelddata.deviceid ?? ""
                               ]
        print(dic)
        
        let serviceName = "userLogin"
        serviceManager.ServiceName = serviceName
        serviceManager.Parameters = dic
        serviceManager.MakeServiceCall(httpMethod: .post) { (UserResponse) in
            print(UserResponse)
            
            let userResponse = UserResponse as? ResponseModel<LoginData>
            Login = userResponse?.Data
         
            if Login?.count == 0{
                
            }else{
               // UserDefaults.standard.set(true, forKey: "login")
                UserDefaults.standard.set(Login?[0].loginToken, forKey: "token")
                UserDefaults.standard.set(Login?[0].id, forKey: "userid")
            }
          //  UserDefaults.standard.se
            if UserDefaults.standard.value(forKey: "login") as? Bool ?? false  == false{
                let push = self.storyboard?.instantiateViewController(withIdentifier: "termsandcondi") as! termsandcondi
                self.navigationController?.pushViewController(push, animated: true)
            
            }else{
                let push = self.storyboard?.instantiateViewController(withIdentifier: "mytabbar") as! mytabbar
                push.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(push, animated: true)
            }
            
            
//            UIAlertController().Simplealert(withTitle: userResponse?.errorMsg ?? "", Message: "", presentOn: self)
            
//            UIAlertController().simpleMessageWithSingleAction(action: {
//                let push = self.storyboard?.instantiateViewController(withIdentifier: "mytabbar") as! mytabbar
//                           //        push.DataToPassRegister = [:]
//                           //        let backItem = UIBarButtonItem()
//                           //        backItem.title = ""
//                           //        navigationItem.backBarButtonItem = backItem // This will show in the
//                           self.navigationController?.pushViewController(push, animated: true)
//            }, title: userResponse?.errorMsg ?? "", message: "", presentOn: self)
           
        }
    }
    
    func apiCheckSocialLogin(email : String){
        let serviceManager = ServiceManager<socialLoginResponse>.init()
        let dic:[String:Any] = ["email":email,
                                "device_id" : objModelddata.deviceid ?? ""
    ]
      print(dic)
        
        let serviceName = "checkFbGoogleLogin"
        serviceManager.ServiceName = serviceName
        serviceManager.Parameters = dic
        serviceManager.HandleResponse = false
        serviceManager.MakeServiceCall(httpMethod: .post) { (UserResponse) in
            
            if serviceManager.HandleResponse == false{
                print("false")
            }else{
                 print("true")
            }
            print(UserResponse)
            
            let userResponse = UserResponse as? ResponseModel<socialLoginResponse>
            self.socialLoogin = userResponse?.Data
           // if self.socialLoogin?.count == 0{
                          
                 //     }else{
                         // UserDefaults.standard.set(true, forKey: "login")
                      
                    
         //   UIAlertController().Simplealert(withTitle: userResponse?.errorMsg ?? "", Message: "", presentOn: self)
            if userResponse?.status == "0"{
                
                let push = self.storyboard?.instantiateViewController(withIdentifier: "RegisterPageController") as! RegisterPageController
                self.navigationController?.pushViewController(push, animated: true)
            }else{
                UserDefaults.standard.set(self.socialLoogin?[0].loginToken, forKey: "token")
                UserDefaults.standard.set(  self.socialLoogin?[0].id, forKey: "userid")
                if UserDefaults.standard.value(forKey: "login") as? Bool ?? false  == false{
                    let push = self.storyboard?.instantiateViewController(withIdentifier: "termsandcondi") as! termsandcondi
                    self.navigationController?.pushViewController(push, animated: true)
                    
                }else{
                    let push = self.storyboard?.instantiateViewController(withIdentifier: "mytabbar") as! mytabbar
                    push.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(push, animated: true)
                }
            //}
              }
        }
    }
}

//
//  ChoosecityVC.swift
//  Cityguide
//
//  Created by DECODER on 06/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

protocol selectAddressDelegate {
    func selectedData(name : String)
}

class ChoosecityVC: UIViewController, UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate,GMSMapViewDelegate {
    
    var searchCityData : [cityData]?
    var addressDelegate : selectAddressDelegate?
    var fromController : String!
    
    @IBOutlet weak var searchingtxt: UITextField!
    @IBOutlet weak var searchingtb: UITableView!
    @IBOutlet weak var searchingview: UIView!
    @IBOutlet weak var tblCityView: UIView!
    @IBOutlet var chooseview: UIView!
    @IBOutlet weak var lblValidation: UILabel!
    
    @IBOutlet weak var heightConstasnt: NSLayoutConstraint!
    @IBOutlet weak var viewheightConstasnt: NSLayoutConstraint!
 
    @IBOutlet weak var closeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblCityView.layer.cornerRadius = 20
        tblCityView.clipsToBounds = true
        //   heightConstasnt.constant = 0
        // tblCityView.backgroundColor = .clear
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.isNavigationBarHidden = true
        
        self.chooseview.applyGradient1(colours: [UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1),UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1)],locations: [0.0,1.0])
        
        lblValidation.isHidden = true
        
        let leftView = UIView(frame: CGRect(x: 00.0, y: 0.0, width: 20.0, height: 2.0))
        searchingtxt.leftView = leftView
        searchingtxt.leftViewMode = .always
        
        searchingtb.register(UINib(nibName: "Citycell", bundle: nil), forCellReuseIdentifier: "Citycell")
        searchingtb.delegate = self
        searchingtb.dataSource = self
        
        searchingtxt.layer.cornerRadius = searchingtxt.frame.size.height / 2
        searchingtxt.clipsToBounds = true
        
        // searchingtb.frame = CGRect(x: searchingtb.frame.origin.x, y: searchingtb.frame.origin.y, width: searchingtb.frame.size.width, height: searchingtb.contentSize.height)
        searchingtxt.layer.borderWidth = 0
        closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
    }
    
    @objc func closeButtonPressed(){
        self.searchingtxt.text = ""
        self.tblCityView.isHidden = true
        //   lblValidation.isHidden = false
        lblValidation.hideWithAnimation(hidden: false)
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchCityData?.count == nil{
            return 0
        }else{
            return searchCityData?.count ?? 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = searchingtb.dequeueReusableCell(withIdentifier: "Citycell") as! Citycell
        cell.citynamelbl.text = (searchCityData?[indexPath.row].allName ?? "")
        cell.selectionStyle = .none
        
        return cell
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print((textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? "")
        if ((textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? "").count >= 3 {
            
            //  if textField.text?.count ?? 0 >= 2{
            DispatchQueue.main.async {
                self.apiGetSerachCityData(search: ((textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? "").trimmingCharacters(in: .whitespaces))
            }
            lblValidation.hideWithAnimation(hidden: true)
            // lblValidation.isHidden = true
        }else{
            //   lblValidation.isHidden = false
            lblValidation.hideWithAnimation(hidden: false)
            self.tblCityView.isHidden = true
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        if textField.text?.count ?? 0 >= 3{
           // apiGetSerachCityData(search: (textField.text ?? "").trimmingCharacters(in: .whitespaces))
        }else{
            // apiGetSerachCityData(search: textField.text ?? "")
            //  lblValidation.isHidden = false
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text?.count ?? 0 >= 3{
                   apiGetSerachCityData(search: (textField.text ?? "").trimmingCharacters(in: .whitespaces))
               }else{
                   // apiGetSerachCityData(search: textField.text ?? "")
                   //  lblValidation.isHidden = false
               }
      
        return true
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == searchingtb{
            //heightConstasnt.constant = tableView.contentSize.height
            //  viewheightConstasnt.constant = tableView.contentSize.height
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if fromController == "profile"{
            self.addressDelegate?.selectedData(name: searchCityData?[indexPath.row].allName ?? "")
            self.dismiss(animated: true, completion: nil)
        }else{
        print(searchCityData?[indexPath.row].allName ?? "")
        UserDefaults.standard.set(searchCityData?[indexPath.row].allName ?? "", forKey: "city")
        apiAddCity(stateid: searchCityData?[indexPath.row].stateID ?? "", city_id: searchCityData?[indexPath.row].id ?? "", country_id: searchCityData?[indexPath.row].countryID ?? "", name: searchCityData?[indexPath.row].name ?? "")
        }
    }
    
}

extension UIView
{
    func applyGradient1(colours: [UIColor]) -> Void {
        self.applyGradient1(colours: colours, locations: nil)
    }
    
    func applyGradient1(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func hideWithAnimation(hidden: Bool) {
        UIView.transition(with: self, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.isHidden = hidden
        })
    }
}

extension ChoosecityVC{
    //MARK:-  cityData all city list
    func apiGetSerachCityData(search : String){
        
        let serviceManager = ServiceManager<cityData>.init()
        let dic:[String:Any] = ["city":search,
                                "token" : UserDefaults.standard.value(forKey: "token") as? String ?? ""]
        print(dic)
        
        let serviceName = "userSearchcity"
        serviceManager.ServiceName = serviceName
        serviceManager.Parameters = dic
        serviceManager.ShowLoader = false
        serviceManager.MakeServiceCall(httpMethod: .get) { (UserResponse) in
            print(UserResponse)
            self.tblCityView.isHidden = false
            let userResponse = UserResponse as? ResponseModel<cityData>
            self.searchCityData = userResponse?.Data
            self.searchingtb.isHidden = false
            
            self.searchingtb.reloadData()
            self.viewheightConstasnt.constant = self.searchingtb.contentSize.height
            self.heightConstasnt.constant = self.searchingtb.contentSize.height
        }
    }
    
    // MARK:-  add city
    func apiAddCity(stateid:String,city_id:String,country_id:String,name:String){
        
        let serviceManager = ServiceManager<addCity>.init()
        let dic:[String:Any] = ["state_id":stateid,
                                "city_id":city_id,
                                "country_id":country_id,
                                "user_id":UserDefaults.standard.value(forKey: "userid") as? String ?? "",
                                "name":name,
                                "token":UserDefaults.standard.value(forKey: "token") as? String ?? ""]
        
        print(dic)
        let serviceName = "userAddCity"
        serviceManager.ServiceName = serviceName
        serviceManager.Parameters = dic
        serviceManager.ShowLoader = true
        
        serviceManager.MakeServiceCall(httpMethod: .get) { (UserResponse) in
            print(UserResponse)
            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "intresetedVC") as? intresetedVC else {return}
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

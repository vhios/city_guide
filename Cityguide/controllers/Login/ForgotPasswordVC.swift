//
//  ForgotPasswordVC.swift
//  Cityguide
//
//  Created by Maitree on 28/01/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    @IBOutlet var viewBgGradient: UIView!
    @IBOutlet weak var txtEmail: RdtextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var viewInfo: UIView!
    
    var responseDta : [ResponseModelSuccess]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewBgGradient.applyGradient(colours: [UIColor(red: 231/255, green: 59/255, blue: 61/255, alpha: 1), UIColor(red: 241/255, green: 91/255, blue: 93/255, alpha: 1)])
               self.viewBgGradient.clipsToBounds = true
               
               txtEmail.layer.cornerRadius = txtEmail.frame.height / 2.2
               txtEmail.clipsToBounds = true
               
               viewInfo.layer.cornerRadius = 12
               viewInfo.clipsToBounds = true
        
                btnNext.layer.cornerRadius = btnNext.frame.height / 2.2
                btnNext.clipsToBounds = true
               
              // navigationItem.hidesBackButton = true
        // Do any additional setup after loading the view.
    }
    
     override func viewWillAppear(_ animated: Bool) {
           self.navigationController?.setNavigationBarHidden(false, animated: true)
                  var backbutton = UIBarButtonItem()
                  self.navigationItem.hidesBackButton = true
                  backbutton = UIBarButtonItem.init(image: #imageLiteral(resourceName: "left-arrow"), style: .plain, target: self, action: #selector(actionback))
                  self.navigationItem.leftBarButtonItem = backbutton
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        //   (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "Terms & Conditions", right: false,profile:false)
       }
    
    
    override func viewDidDisappear(_ animated: Bool) {
//        self.navigationController?.navigationBar.setBackgroundImage(nil, for:.default)
//        self.navigationController?.navigationBar.shadowImage = nil
//        self.navigationController?.navigationBar.layoutIfNeeded()
    }
    
    @objc func actionback(){
        self.navigationController?.popViewController(animated: true)
//       {
//           print((self.navigationController?.topViewController as! RegisterPageController).controlller)
//           (self.navigationController?.topViewController as! RegisterPageController).setViewControllers([(self.navigationController?.topViewController as! RegisterPageController).controlller[0]], direction: .reverse, animated: true, completion: nil)
//           (self.navigationController?.topViewController as! RegisterPageController).pagecontrol.currentPage = 0
    }
//
    @IBAction func btnNext(_ sender: UIButton) {
        if txtEmail.text?.isEmpty == true{
            UIAlertController().Simplealert(withTitle: "Please enter email", Message: "", presentOn: self)
            //txtFirstName.setTitleVisible(true, animated: true, animationCompletion: nil)
        }
        else if txtEmail.isValidEmail(txtEmail.text ?? "") == false{
            UIAlertController().Simplealert(withTitle: "Please enter valid email", Message: "", presentOn: self)
            
        }else{
        
            apiForgotPassword()
        }
    }
}

extension ForgotPasswordVC{
    //MARK:- apicall for forgot password
    func apiForgotPassword(){
       
        let serviceManager = ServiceManager<ResponseModelSuccess>.init()
        let dic:[String:Any] = ["email":objModelddata.email ?? ""]
        print(dic)
        
        let serviceName = "forgotPassword"
        serviceManager.ServiceName = serviceName
        serviceManager.Parameters = dic
        serviceManager.MakeServiceCall(httpMethod: .post) { (UserResponse) in
            print(UserResponse)
            
            let userResponse = UserResponse as? ResponseModel<ResponseModelSuccess>
            self.responseDta = userResponse?.Data
            
            UIAlertController().Simplealert(withTitle: userResponse?.errorMsg ?? "", Message: "", presentOn: self)
            
//            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "TurnonnotificationVC") as? TurnonnotificationVC else {return}
//            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}




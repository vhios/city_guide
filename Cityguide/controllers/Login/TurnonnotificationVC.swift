//
//  TurnonnotificationVC.swift
//  Cityguide
//
//  Created by DECODER on 07/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class TurnonnotificationVC: UIViewController {
    @IBOutlet weak var yesbtn: UIButton!
    @IBOutlet weak var notifactionheadinglbl: UILabel!
    @IBOutlet weak var notificationbellview: UIView!
    @IBOutlet weak var notificationview: UIView!
    @IBOutlet var notificationbackview: UIView!
    
    var registerData : RegisterData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.notificationbackview.applyGradient1(colours: [UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1),UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1)],locations: [1.0,0.0])
        
       notificationview.layer.cornerRadius = 20
       // notificationbellview.layer.cornerRadius = notificationbellview.frame.height / 2
      // notificationbellview.layer.borderWidth = 2
       // notificationbellview.layer.borderColor = UIColor.red.cgColor
        
        yesbtn.layer.cornerRadius = yesbtn.frame.height / 2
        self.navigationItem.hidesBackButton = true
        yesbtn.layer.shadowColor = UIColor(red: 2/255, green: 0, blue: 0, alpha: 0.25).cgColor
        yesbtn.layer.shadowOffset = CGSize(width: 3, height: 3)
        yesbtn.layer.shadowOpacity = 1.0
        yesbtn.layer.shadowRadius = 10.0
        yesbtn.layer.masksToBounds = true
        
        self.navigationItem.hidesBackButton = true

        self.yesbtn.applyGradient1(colours: [UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1),UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1)],locations: [1.0,0.0])
        
    //   mynavigation().setupNavBack(onVC: self, withTitle: "", right: false,left:false)
      //  (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "", right: true,profile:true,left:true)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
//    }
//
    
    @IBAction func skipbtn(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "login")

        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChoosecityVC") as? ChoosecityVC else {return}
        self.navigationController?.pushViewController(vc, animated: true)
        //apiTurnOnNotification(notificationStatus: "0")
    }
    
    @IBAction func Yes(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "login")
        apiTurnOnNotification(notificationStatus: "1")
    }
    
}

extension TurnonnotificationVC{
    //MARK:- APi turn notification
    
    func apiTurnOnNotification(notificationStatus : String){
        
        let serviceManager = ServiceManager<ResponseModelSuccess>.init()
        let dic:[String:Any] = ["userid":registerData?.id ?? "",
                                "status" : notificationStatus,
                                "token" : UserDefaults.standard.value(forKey: "token") as? String ?? ""]
        print(dic)
        
        let serviceName = "userNotificationChange"
        serviceManager.ServiceName = serviceName
        serviceManager.Parameters = dic
        serviceManager.MakeServiceCall(httpMethod: .post) { (UserResponse) in
            print(UserResponse)
            
            let userResponse = UserResponse as? ResponseModel<ResponseModelSuccess>
            // self.responseDta = userResponse?.Data
            
            
            UIAlertController().simpleMessageWithSingleAction(action: {
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChoosecityVC") as? ChoosecityVC else {return}
                self.navigationController?.pushViewController(vc, animated: true)
            }, title: userResponse?.errorMsg ?? "", message: "", presentOn: self)
            
            //           guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "mytabbar") as? mytabbar else {return}
            //   self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

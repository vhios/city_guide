//
//  intresetedVC.swift
//  Cityguide
//
//  Created by Maitree on 05/02/20.
//  Copyright © 2020 DECODER. All rights reserved.
//

import UIKit

class intresetedVC: UIViewController {

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var objintresteed: UICollectionView!
    @IBOutlet weak var btnCancle: UIButton!
    @IBOutlet weak var textfieldSearch: UITextField!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!

    var from : String?
    var catData : [CategoryDataResponce]?
    var oldContentOffset = CGPoint.zero
    let topConstraintRange = (CGFloat(0)..<CGFloat(140))
    static var scrollViewDidScrollPreviousOffset: CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        objintresteed.register(UINib.init(nibName: "intrestedHeaderCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "intrestedHeaderCell")
        
        objintresteed.register(UINib.init(nibName: "interestedCollectionCell", bundle: nil), forCellWithReuseIdentifier: "interestedCollectionCell")
        
        objintresteed.allowsMultipleSelection = true
        if from == "rootview"{
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            self.navigationController?.isNavigationBarHidden = true
        }else{
            self.navigationController?.setNavigationBarHidden(false, animated: false)
            self.navigationController?.isNavigationBarHidden = false
            self.navigationItem.hidesBackButton = true
            
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default);
            self.navigationController?.navigationBar.shadowImage = UIImage();
            mynavigation().setupNavBack(onVC: self, withTitle: "", right: false,left:false)
          //  self.navigationController?.navigationBar.isTranslucent = true;
            var backbutton = UIBarButtonItem()
            self.navigationItem.hidesBackButton = true
            backbutton = UIBarButtonItem.init(image: #imageLiteral(resourceName: "left-arrow"), style: .plain, target: self, action: #selector(actionback))
            self.navigationItem.leftBarButtonItem = backbutton
                   
            
          //  (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "", right: false,left:true)
        }
        
        let leftView = UIView(frame: CGRect(x: 00.0, y: 0.0, width: 20.0, height: 2.0))
        textfieldSearch.leftView = leftView
        textfieldSearch.leftViewMode = .always
        
        textfieldSearch.layer.cornerRadius = 20
        textfieldSearch.clipsToBounds = true
        
        textfieldSearch.addShadowToTextField(cornerRadius: textfieldSearch.frame.size.height / 2)
        apiAllGategory()
    }
    
    @objc func actionback(){
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- UIButton Action
    @IBAction func btnActionDone(_ sender: UIButton) {
        let push = self.storyboard?.instantiateViewController(withIdentifier: "mytabbar") as! mytabbar
        self.navigationController?.pushViewController(push, animated: true)
        print("Done")
        UserDefaults.standard.set(catData?[0].subCat?[0].name, forKey: "interest")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true

    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView == objintresteed{
//           let offset = scrollView.contentOffset.y
//        print(offset)
//        if offset > 0.0{
//            self.heightConstraint.constant = 0
//            self.searchView.frame = CGRect(x: 0, y: 30, width: self.view.bounds.size.width, height:40)
//             self.objintresteed.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height:self.view.bounds.size.height)
//
//        }else{
//            if offset > 50{
//                self.heightConstraint.constant = 0
//                self.searchView.frame = CGRect(x: 0, y: 30, width: self.view.bounds.size.width, height:40)
//                 self.objintresteed.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height:self.view.bounds.size.height)
//            }else{
//                self.heightConstraint.constant = 135
//                self.searchView.frame = CGRect(x: 0, y: 198, width: self.view.bounds.size.width, height:40)
//                self.objintresteed.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height:self.view.bounds.size.height - 200)
//            }
//
//        }
//        }
//    }
}
//MARK:- UIcollectionView datasource & delegate method s
extension intresetedVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return catData?.count ?? 0
    }
     func collectionView(_ collectionView: UICollectionView,
                       viewForSupplementaryElementOfKind kind: String,
                       at indexPath: IndexPath) -> UICollectionReusableView {
        
        guard let cell  = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "intrestedHeaderCell", for: indexPath) as? intrestedHeaderCell else {return UICollectionReusableView()}
        if indexPath.section == 0{
            cell.lblHeaderView.isHidden = true
        }else{
             cell.lblHeaderView.isHidden = false
        }
        cell.lblHeaderName.text = catData?[indexPath.section].name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

       // if catData?[section].subCat?.count == 1{
               return CGSize(width: 0, height: 60)
//        }else{
//               return CGSize(width: collectionView.frame.width / 3, height: 60)
//        }

    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
//        if(indexPath.section == 1)
//        {
//            return CGSize(width: 0, height: 60)
//        }else{
//            return CGSize(width: 200, height: 60)
//        }
        
        let label = UILabel(frame: CGRect.zero)
        label.text = catData?[indexPath.section].subCat?[indexPath.row].name
        label.sizeToFit()
        return CGSize(width: label.frame.width + 22, height: 40)
//        let width = collectionView.frame.width / 4 - 15
//        return CGSize(width: width, height: 40)
      
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return catData?[section].subCat?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "interestedCollectionCell", for: indexPath) as? interestedCollectionCell else {return UICollectionViewCell()}
        
        cell.lblName.text = catData?[indexPath.section].subCat?[indexPath.row].name
        cell.borderView.layer.cornerRadius = 10
        cell.borderView.clipsToBounds = true
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = objintresteed.cellForItem(at: indexPath) as? interestedCollectionCell
        cell?.borderView.backgroundColor = UIColor(red: 231/255, green: 59/255, blue: 61/255, alpha: 1)
      
        cell?.lblName.textColor = UIColor.white
       
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
         let cell = objintresteed.cellForItem(at: indexPath) as? interestedCollectionCell
               cell?.borderView.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        
        cell?.lblName.textColor = UIColor.black
        
    }
}

extension intresetedVC{
    func apiAllGategory(){
       
        let serviceManager = ServiceManager<CategoryDataResponce>.init()
        let dic:[String:Any] = ["token" : UserDefaults.standard.value(forKey: "token") as? String ?? ""]
            print(dic)
            
            let serviceName = "allCategoty"
            serviceManager.ServiceName = serviceName
            serviceManager.Parameters = dic
            serviceManager.MakeServiceCall(httpMethod: .get) { (UserResponse) in
                print(UserResponse)
              //  self.objintresteed.isHidden = false
                let userResponse = UserResponse as? ResponseModel<CategoryDataResponce>
                self.catData = userResponse?.Data
                self.objintresteed.reloadData()
               
            }
        }
}



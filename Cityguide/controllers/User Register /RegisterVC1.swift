//
//  RegisterVC1.swift
//  CityGuideApp
//
//  Created by DECODER on 07/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class RegisterVC1: UIViewController {

    @IBOutlet var viewBgGradient: UIView!
    @IBOutlet weak var txtFirstName: RdtextField!
    @IBOutlet weak var txtLastName: RdtextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var viewInfo: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        self.viewBgGradient.applyGradient(colours: [UIColor(red: 231/255, green: 59/255, blue: 61/255, alpha: 1), UIColor(red: 241/255, green: 91/255, blue: 93/255, alpha: 1)])
        self.viewBgGradient.clipsToBounds = true
        
        txtFirstName.layer.cornerRadius = txtFirstName.frame.height / 2.2
        txtFirstName.clipsToBounds = true
        
        txtLastName.layer.cornerRadius = txtLastName.frame.height / 2.2
        txtLastName.clipsToBounds = true
        
        viewInfo.layer.cornerRadius = 12
        viewInfo.clipsToBounds = true
        
        self.navigationItem.hidesBackButton = true
        
        if objModelddata.type == nil{
            txtFirstName.text = ""
            txtLastName.text = ""
        }else{
            self.txtFirstName.text = objModelddata.firstName
            self.txtLastName.text = objModelddata.lastName
        }
        
//        let back = UIBarButtonItem.init(image: #imageLiteral(resourceName: "back-arrow"), style: .plain, target: self, action: #selector(actionback))
//        self.navigationController?.navigationItem.leftBarButtonItem = back
        
        // Do any additional setup after loading the view.
    }
    
//    @objc func actionback()
//    {
//        print((self.navigationController?.topViewController as! RegisterVC).controlller)
//        (self.navigationController?.topViewController as! RegisterVC).setViewControllers([(self.navigationController?.topViewController as! RegisterVC).controlller[0]], direction: .reverse, animated: true, completion: nil)
//        (self.navigationController?.topViewController as! RegisterVC).pagecontrol.currentPage = 0
//
//    }
    
    @IBAction func btnNextPressed(_ sender: UIButton) {
        if txtFirstName.text?.isEmpty == true{
             UIAlertController().Simplealert(withTitle: "Please enter First name", Message: "", presentOn: self)
        }
        else if txtFirstName.isValid(testStr: (txtFirstName.text ?? "").trimmingCharacters(in: .whitespaces)) == false{
             UIAlertController().Simplealert(withTitle: "Please enter valid name", Message: "", presentOn: self)
            
        }
        else if txtLastName.text?.isEmpty == true{
             UIAlertController().Simplealert(withTitle: "Please enter Last name", Message: "", presentOn: self)
          
        }else if txtLastName.isValid(testStr: (txtLastName.text ?? "").trimmingCharacters(in: .whitespaces)) == false{
            UIAlertController().Simplealert(withTitle: "Please enter valid last name", Message: "", presentOn: self)
           
        }
        else{
            objModelddata.firstName = txtFirstName.text ?? ""
            objModelddata.lastName = txtLastName.text ?? ""

            print((self.navigationController?.topViewController as! RegisterPageController).controlller)
            (self.navigationController?.topViewController as! RegisterPageController).setViewControllers([(self.navigationController?.topViewController as! RegisterPageController).controlller[1]], direction: .forward, animated: true, completion: nil)
            //        (self.navigationController?.topViewController as! RegisterPageController).DataToPassRegister["first_name"] = .text
            //        (self.navigationController?.topViewController as! RegisterPageController).DataToPassRegister["last_name"] = txtlastname.text
            (self.navigationController?.topViewController as! RegisterPageController).pagecontrol.currentPage = 1
            
        }
    }
}

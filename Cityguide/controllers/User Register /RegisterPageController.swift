//
//  RegisterPageController.swift
//  CityGuideApp
//
//  Created by DECODER on 07/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class RegisterPageController: UIPageViewController {
    
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    var controlller:[UIViewController] = {
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        let vc1 = storyboard1.instantiateViewController(withIdentifier: "RegisterVC1")
        let vc2 = storyboard1.instantiateViewController(withIdentifier: "RegisterVC2")
        let vc3 = storyboard1.instantiateViewController(withIdentifier: "RegisterVC3")
        let vc4 = storyboard1.instantiateViewController(withIdentifier: "RegisterVC4")
        return [vc1,vc2,vc3,vc4]
    }()
    var backbutton = UIBarButtonItem()
    var pagecontrol = UIPageControl()
    
    fileprivate func getViewController(withIdentifier identifier: String) -> UIViewController
    {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //            self.dataSource = self
        //            self.delegate = self
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        
        self.navigationItem.hidesBackButton = true
        backbutton = UIBarButtonItem.init(image: #imageLiteral(resourceName: "left-arrow"), style: .plain, target: self, action: #selector(actionback))
        
        self.navigationItem.leftBarButtonItem = backbutton
        
        if let firstview = controlller.first
        {
            self.setViewControllers([firstview], direction:.forward, animated: true, completion: nil)
        }
    
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default);
        self.navigationController?.navigationBar.shadowImage = UIImage();
        self.navigationController?.navigationBar.isTranslucent = true;
        // self.navigationController?.view.backgroundColor = .clear
        //
        
        self.Configepagecontrol()
    }
    
    func Configepagecontrol()
    {
        pagecontrol.isUserInteractionEnabled = true
        
        if UIDevice.modelName == "iPhone 5" || UIDevice.modelName == "iPhone 5c" || UIDevice.modelName == "iPhone 5s" || UIDevice.modelName == "iPhone SE"
        {
            pagecontrol = UIPageControl.init(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY - 50, width: UIScreen.main.bounds.width, height: 50))
        }
        else
        {
            pagecontrol = UIPageControl.init(frame: CGRect(x: 0, y: UIScreen.main.bounds.maxY/2 + 150, width: UIScreen.main.bounds.width, height: 50))
        }
        //
        pagecontrol.numberOfPages = controlller.count
        pagecontrol.currentPage = 0
        pagecontrol.tintColor = UIColor.black
        pagecontrol.pageIndicatorTintColor = UIColor.white.withAlphaComponent(0.5)
        pagecontrol.currentPageIndicatorTintColor = UIColor.white
        self.view.addSubview(pagecontrol)
    }
    
    @objc func actionback(view: UIViewController)
    {
        print(controlller.count)
        if pagecontrol.currentPage == 0 {
            self.navigationController?.popViewController(animated: true)
        }
            
        else if pagecontrol.currentPage == 1
        {
            (self.navigationController?.topViewController as! RegisterPageController).setViewControllers([(self.navigationController?.topViewController as! RegisterPageController).controlller[0]], direction: .reverse, animated: true, completion: nil)
            
            pagecontrol.currentPage = 0
        }
            
        else if pagecontrol.currentPage == 2
        {
            (self.navigationController?.topViewController as! RegisterPageController).setViewControllers([(self.navigationController?.topViewController as! RegisterPageController).controlller[1]], direction: .reverse, animated: true, completion: nil)
            
            pagecontrol.currentPage = 1
        }
            
        else if pagecontrol.currentPage == 3
        {
            (self.navigationController?.topViewController as! RegisterPageController).setViewControllers([(self.navigationController?.topViewController as! RegisterPageController).controlller[2]], direction: .reverse, animated: true, completion: nil)
            
            pagecontrol.currentPage = 2
        }
        
    }
    
}

//
//  termsandcondi.swift
//  Cityguide
//
//  Created by DECODER on 09/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class termsandcondi: UIViewController {
    
    @IBOutlet weak var termtxt: UITextView!
    @IBOutlet weak var acceptbtn: UIButton!
    @IBOutlet weak var disagreeBtn: UIButton!
    
    var fromController: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if fromController == "true"{
            self.acceptbtn.isHidden = true
            self.disagreeBtn.isHidden = true
            
        }else{
            self.acceptbtn.isHidden = false
             self.disagreeBtn.isHidden = false

        }
        self.acceptbtn.titleLabel?.textColor = UIColor.white
        self.disagreeBtn.titleLabel?.textColor = UIColor.white
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.hidesBackButton = true
           if fromController == "true"{
              mynavigation().setupNavBack(onVC: self, withTitle: "Terms & Conditions", right: false,left:true)
           }else{
             mynavigation().setupNavBack(onVC: self, withTitle: "Terms & Conditions", right: false,left:false)
        }
        
                        
     //   (self.navigationController as? mynavigation)?.setupNavBack(onVC: self, withTitle: "Terms & Conditions", right: false,profile:false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//          self.navigationController?.navigationBar.setBackgroundImage(nil, for:.default)
//          self.navigationController?.navigationBar.shadowImage = nil
//          self.navigationController?.navigationBar.layoutIfNeeded()
        //self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.acceptbtn.applyGradientBtn(colors: [UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1).cgColor,UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1).cgColor])
        
        self.disagreeBtn.applyGradient(colours: [UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1),UIColor(red: 241.0/255.0, green: 91.0/255.0, blue: 93.0/255.0, alpha: 1)],locations: [0.0,1.0])
    }
    
    @IBAction func agreedbutton(_ sender: Any) {
        
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "TurnonnotificationVC") as? TurnonnotificationVC else {return}
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func disagreeBtn(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
//        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else {return}
//        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension UIColor {
    static func gradientColor(startColor: UIColor, endColor: UIColor, frame: CGRect) -> UIColor? {
        let gradient = CAGradientLayer(layer: frame.size)
        gradient.frame = frame
        gradient.colors = [startColor.cgColor, endColor.cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        UIGraphicsBeginImageContext(frame.size)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        gradient.render(in: context)
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return UIColor(patternImage: image)
    }
}

//
//  RegisterVC4.swift
//  CityGuideApp
//
//  Created by DECODER on 07/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class RegisterVC4: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet var viewBgGradient: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var btnEditProfileImg: UIButton!
    @IBOutlet weak var txtUserName: RdtextField!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var viewInfo: UIView!
    
    var register:[RegisterData]?
      let imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewBgGradient.applyGradient(colours: [UIColor(red: 231/255, green: 59/255, blue: 61/255, alpha: 1), UIColor(red: 241/255, green: 91/255, blue: 93/255, alpha: 1)])
        self.viewBgGradient.clipsToBounds = true
        
        imgProfile.layer.cornerRadius = imgProfile.frame.height / 2
        imgProfile.layer.borderWidth = 1.0
        imgProfile.layer.borderColor = UIColor(red: 231/255, green: 59/255, blue: 61/255, alpha: 1).cgColor
        imgProfile.clipsToBounds = true
        
        btnEditProfileImg.layer.cornerRadius = btnEditProfileImg.frame.height / 2
        btnEditProfileImg.clipsToBounds = true
        
        txtUserName.layer.cornerRadius = txtUserName.frame.height / 2.2
        txtUserName.clipsToBounds = true
        
        btnStart.layer.cornerRadius = btnStart.frame.height / 2.2
        btnStart.clipsToBounds = true
        
        viewInfo.layer.cornerRadius = 12
        viewInfo.clipsToBounds = true
        
         imagePicker.delegate = self
        navigationItem.hidesBackButton = true
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.btnEditProfileImg.applyGradient(colours: [UIColor(red: 231/255, green: 59/255, blue: 61/255, alpha: 1), UIColor(red: 241/255, green: 91/255, blue: 93/255, alpha: 1)])
        self.btnEditProfileImg.clipsToBounds = true
        
        self.btnStart.applyGradient(colours: [UIColor(red: 231/255, green: 59/255, blue: 61/255, alpha: 1), UIColor(red: 241/255, green: 91/255, blue: 93/255, alpha: 1)])
        self.btnStart.clipsToBounds = true
    }
    
    @IBAction func btnEditProfileImgPressed(_ sender: UIButton) {
        let alertController = UIAlertController.init(title: "SELECT IMAGE", message: "Please select image", preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Camera", comment: ""), style: .default) { _ in
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Gallery", comment: ""), style: .default) { _ in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { _ in
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // get image method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let info1 = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        imgProfile.image = info1
      
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnStartPressed(_ sender: UIButton) {
        if txtUserName.text?.isEmpty == true{
             UIAlertController().Simplealert(withTitle:"Please enter user name", Message: "", presentOn: self)
                   //txtFirstName.setTitleVisible(true, animated: true, animationCompletion: nil)
        }else if txtUserName.isValidInput(Input: txtUserName.text ?? "") == false{
            UIAlertController().Simplealert(withTitle: "Please enter valid user name", Message: "", presentOn: self)
        }
            else{
            objModelddata.userName = txtUserName.text ?? ""
            apiRegisterData()
         }
    }
    
}
extension RegisterVC4{
    //MARK:- apicall for register data
    func apiRegisterData(){
        
         let serviceManager = ServiceManager<RegisterData>.init()
        let dic:[String:Any] = ["first_name": objModelddata.firstName ?? "",
                                "last_name": objModelddata.lastName ?? "",
                                "email":objModelddata.email ?? "",
                                "password":objModelddata.password ?? "",
                                "username":objModelddata.userName ?? "",
                                "register_type" : objModelddata.type ?? "normal",
                                "device_type" : "ios",
                                "device_id" : objModelddata.deviceid ?? "",
                                "facebook_id" : objModelddata.facebookId ?? "",
                                "gmail_id" : objModelddata.gogleId ?? "",
                                "apple_id" : objModelddata.appleId ?? ""
        ]
        print(dic)
        
        let serviceName = "registerUser"
        serviceManager.ServiceName = serviceName
        serviceManager.Parameters = dic
        serviceManager.MakeServiceCall(httpMethod: .post) { (UserResponse) in
            print(UserResponse)
            
            let userResponse = UserResponse as? ResponseModel<RegisterData>
            self.register = userResponse?.Data
            
          //  UIAlertController().Simplealert(withTitle: userResponse?.errorMsg ?? "", Message: "", presentOn: self)
            
            UIAlertController().simpleMessageWithSingleAction(action: {
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else {return}
                vc.fromController = "first"
                           self.navigationController?.pushViewController(vc, animated: true)
            }, title: userResponse?.errorMsg ?? "", message: "", presentOn: self)
            
        }
    }
}



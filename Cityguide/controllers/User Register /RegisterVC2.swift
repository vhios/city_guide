//
//  RegisterVC2.swift
//  CityGuideApp
//
//  Created by DECODER on 07/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class RegisterVC2: UIViewController {

    @IBOutlet var viewBgGradient: UIView!
    @IBOutlet weak var txtEmail: RdtextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var viewInfo: UIView!
    var objModel : RegisterUserData = RegisterUserData()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewBgGradient.applyGradient(colours: [UIColor(red: 231/255, green: 59/255, blue: 61/255, alpha: 1), UIColor(red: 241/255, green: 91/255, blue: 93/255, alpha: 1)])
        self.viewBgGradient.clipsToBounds = true
        
        txtEmail.layer.cornerRadius = txtEmail.frame.height / 2.2
        txtEmail.clipsToBounds = true
        
        viewInfo.layer.cornerRadius = 12
        viewInfo.clipsToBounds = true
        
        navigationItem.hidesBackButton = true
        
        if objModelddata.type == ""{
            
        }else{
            self.txtEmail.text = objModelddata.email
            
        }
        // Do any additional setup after loading the view.
    }
    
    @objc func actionback()
    {
        print((self.navigationController?.topViewController as! RegisterPageController).controlller)
        (self.navigationController?.topViewController as! RegisterPageController).setViewControllers([(self.navigationController?.topViewController as! RegisterPageController).controlller[0]], direction: .reverse, animated: true, completion: nil)
        (self.navigationController?.topViewController as! RegisterPageController).pagecontrol.currentPage = 0

    }
    
    @IBAction func btnNext(_ sender: UIButton) {
        if txtEmail.text?.isEmpty == true{
            UIAlertController().Simplealert(withTitle:"Please enter email", Message: "", presentOn: self)
            
                   //txtFirstName.setTitleVisible(true, animated: true, animationCompletion: nil)
                   
               }
        else if txtEmail.isValidEmail(txtEmail.text ?? "") == false{
             UIAlertController().Simplealert(withTitle:"Please enter valid email", Message: "", presentOn: self)
            
        }else{
            
            objModelddata.email = txtEmail.text ?? ""
            
        (self.navigationController?.topViewController as! RegisterPageController).setViewControllers([(self.navigationController?.topViewController as! RegisterPageController).controlller[2]], direction: .forward, animated: true, completion: nil)
        //        (self.navigationController?.topViewController as! RegisterVC).DataToPassRegister["email"] = self.txtEmail.text
        (self.navigationController?.topViewController as! RegisterPageController).pagecontrol.currentPage = 2
        
    }
    }
    
}

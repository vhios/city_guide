//
//  RegisterVC3.swift
//  CityGuideApp
//
//  Created by DECODER on 07/12/19.
//  Copyright © 2019 DECODER. All rights reserved.
//

import UIKit

class RegisterVC3: UIViewController {

    @IBOutlet var viewBgGradient: UIView!
    @IBOutlet weak var txtPassword: RdtextField!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var txtConfirmPassword: RdtextField!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var heightViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btnViewPswd: UIButton!
    @IBOutlet weak var btnViewConfirmPswd: UIButton!

    
    var addRegister : RegisterUserData?
    var fromController = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewBgGradient.applyGradient(colours: [UIColor(red: 231/255, green: 59/255, blue: 61/255, alpha: 1), UIColor(red: 241/255, green: 91/255, blue: 93/255, alpha: 1)])
        self.viewBgGradient.clipsToBounds = true
        
        txtPassword.layer.cornerRadius = txtPassword.frame.height / 2.2
        txtPassword.clipsToBounds = true
        
        txtConfirmPassword.layer.cornerRadius = txtPassword.frame.height / 2.2
        txtConfirmPassword.clipsToBounds = true
        
        viewInfo.layer.cornerRadius = 12
        viewInfo.clipsToBounds = true
        
        if fromController == "true"{
            txtConfirmPassword.isHidden = false
            heightViewConstraint.constant = 290
            lblPassword.text = "Change Password"
            navigationItem.hidesBackButton = false
            
        }else{
            txtConfirmPassword.isHidden = true
            heightViewConstraint.constant = 220
            lblPassword.text = "Create a Password"
            navigationItem.hidesBackButton = false

        }
        txtPassword.isSecureTextEntry = true
        txtConfirmPassword.isSecureTextEntry = true
        // Do any additional setup after loading the view.
    }
    @objc func actionback()
    {
        print((self.navigationController?.topViewController as! RegisterPageController).controlller)
        (self.navigationController?.topViewController as! RegisterPageController).setViewControllers([(self.navigationController?.topViewController as! RegisterPageController).controlller[2]], direction: .reverse, animated: true, completion: nil)
        (self.navigationController?.topViewController as! RegisterPageController).pagecontrol.currentPage = 0

    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.navigationController?.setNavigationBarHidden(false, animated: true)
//        self.navigationController?.navigationBar.isTranslucent = false
//
//        self.navigationController?.navigationBar.barTintColor = UIColor(red: 231.0/255.0, green: 59.0/255.0, blue: 61.0/255.0, alpha: 1)
//
        
    }
    
    
    @IBAction func btnViewPswdPressed(_ sender: UIButton) {
           if(sender.isSelected == true) {
               txtPassword.isSecureTextEntry = false
           } else {
               txtPassword.isSecureTextEntry = true
           }

           sender.isSelected = !sender.isSelected
           
       }
    
    @IBAction func btnViewCnfirmPswdPressed(_ sender: UIButton) {
           if(sender.isSelected == true) {
               txtConfirmPassword.isSecureTextEntry = false
           } else {
               txtConfirmPassword.isSecureTextEntry = true
           }

           sender.isSelected = !sender.isSelected
           
       }
    @IBAction func btnNextPressed(_ sender: UIButton) {
        let characterSet = CharacterSet.init(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if txtPassword.text?.isEmpty == true{
            UIAlertController().Simplealert(withTitle:"Please enter password", Message: "", presentOn: self)
         
            //txtFirstName.setTitleVisible(true, animated: true, animationCompletion: nil)
            
        }
        else if (self.txtPassword.text?.count)! < 7
        {
            
             UIAlertController().Simplealert(withTitle:"Password must contain 8 characters", Message: "", presentOn: self)
            
          
        }
            
        else if txtPassword.text?.rangeOfCharacter(from: characterSet.inverted) == nil
        {
            UIAlertController().Simplealert(withTitle:"Password must contain a special character", Message: "", presentOn: self)
            
          
        }
        else{
            
            objModelddata.password = txtPassword.text ?? ""
            
            (self.navigationController?.topViewController as! RegisterPageController).setViewControllers([(self.navigationController?.topViewController as! RegisterPageController).controlller[3]], direction: .forward, animated: true, completion: nil)
            //        (self.navigationController?.topViewController as! RegisterVC).DataToPassRegister["email"] = self.txtEmail.text
            (self.navigationController?.topViewController as! RegisterPageController).pagecontrol.currentPage = 3
        }
    }

}
